import sys
import json
import pprint
import pickle
import torchfile
import numpy as np

# import gensim
from nltk.corpus import wordnet as wn
# from nltk.stem.wordnet import WordNetLemmatizer

from annoy import AnnoyIndex
sys.path.insert(0, './visual_genome_python_driver')

import src.local as vg

dataDir = "./VisualGenome/"
imageDataDir = "./data/by-id/"

# print "Loading word2vec..."
# w2v = gensim.models.Word2Vec.load_word2vec_format('../word2vec.torch/GoogleNews-vectors-negative300.bin', binary=True)  
# print "Loaded successfully"

N_TREES = 10
obj2imid, imid2obj = {}, {}
fc7_pca = {}
imid2ix, ix2imid, cocoimid2ix = {}, {}, {}
index = {}
fc7_vqa_pca = {}
cocofeats_pca = {}

def build_indexes():
	'''
	Build mapping from objects to image id's.
	'''
	print "Building Index..."
	# scene_graphs = vg.GetSceneGraphs(startIndex=1, endIndex=-1, minRels=1, \
 #                                 	 dataDir=dataDir, imageDataDir=imageDataDir)
 	global obj2imid
 	global fc7_pca
 	global imid2ix
 	global ix2imid
 	global index
 	global cocofeats_pca
 	global cocoimid2ix
 	global imid2obj
	# for ix, scene_graph in enumerate(scene_graphs):
	# 	obj_list = scene_graph.objects
	# 	for obj in obj_list:
	# 		for obj_synset in obj.synsets:
	# 			# print obj_synset_root
	# 			# obj_synset_aug = wn.synset(obj_synset_root).hypernyms()
	# 			# print obj_synset_aug
	# 			# obj_synset_aug.append(wn.synset(obj_synset_root))
	# 			# print obj_synset_aug
	# 			# for obj_synset in obj_synset_aug:
	# 			try:
	# 				if ((str(wn.synset(obj_synset)) in obj2imid.keys())):
	# 					if scene_graph.image.id not in obj2imid[str(wn.synset(obj_synset))]:
	# 						obj2imid[str(wn.synset(obj_synset))].append(scene_graph.image.id)
	# 				else:
	# 					obj2imid[str(wn.synset(obj_synset))] = [scene_graph.image.id]
	# 			except Exception as e:
	# 				print "Exception"
	# pickle.dump( obj2imid, open("obj2imid.p", "wb") )

	obj2imid_file = open("obj2imid.p", "rb")
	obj2imid = pickle.load(obj2imid_file)
	fc7_pca = np.load("visgen/fc7_pca.npy")
	fc7_imid = torchfile.load("visgen/val_fc7_image_id.t7")

	cocolist = torchfile.load("../toaim/data/val_fc7_image_id.t7")
	cocofeats_pca = np.load("../toaim/cocofeats_pca.npy")
	# print "Building NN index..."
	index = AnnoyIndex(100, metric="euclidean")
	for ix, imid in enumerate(fc7_imid):
		imid2ix[imid] = ix
		ix2imid[ix] = imid

	for ix, imid in enumerate(cocolist):
		cocoimid2ix[imid] = ix
	# index.add_item(ix, fc7_pca[ix])
	# index.build(N_TREES)
	# index.save("index.ann")
	# print cocoimid2ix.keys()[1:10]
	index.load("index.ann")

	imid2obj = pickle.load(open("imid2obj.p", "rb"))
	print "All indexes built."

def resolve(obj):
	'''
	Return closest object class from Genome annotations to input string
	'''	
	# TODO: Add word sense disambiguation, alternatively CONSTRAIN the POS
	resolved = wn.synsets(obj.lower())
	for syn in resolved:
		# print syn
		if str(syn) in obj2imid:
			# print obj2imid[str(syn)]
			return str(syn)
	return False
	# print str(resolved[0])
	# return str(resolved[0]) if (len(resolved)>0 and (str(resolved[0]) in obj2imid)) else False # Assuming order is based on popularity of usage

def retrieve_neg_img_list(obj_list, source_imid):
	'''
	Return image with k-1, without kth for each k=1..len(obj_list),
	closest in fc7 to source.
	'''
	neg_img_list = []

	# pp = pprint.PrettyPrinter(indent=4)
	# pp.pprint(obj2imid)
	for ix, obj in enumerate(obj_list):
		if len(obj_list) == 1:
			continue
			include = []
			exclude = obj_list
		elif len(obj_list) == 2:
			include = obj_list[:ix] + obj_list[(ix+1):]
			exclude = [obj]
		else:
			return False

		print "Include ", include, " Exclude", exclude
		if (not resolve(exclude[0])) or (0 in include and not resolve(include[0])):
			continue

		# print set(obj2imid[resolve(include[0])])
		# print set(obj2imid[resolve(exclude[0])])

		if not resolve(include[0]):
			continue
		img_list = set(obj2imid[resolve(include[0])])
		for obj in range(1, len(include)):
			if not resolve(obj):
				continue
			img_list = set(obj2imid[resolve(obj)]) & img_list # Skip things we can't resolve for now
		
		# print img_list
		if 0 in exclude:
			img_list = img_list - set(obj2imid[resolve(exclude[0])]) # Remove excluded object
		# print "Length of list", str(len(img_list))
		# print "List:", img_list
		# img_fc7_list = []
		# for img in img_list:
		# 	ix = imid2ix[str(img) + ".jpg"]
		# 	print ix
		# 	if ix in fc7_pca:
		# 		img_fc7_list.append(fc7_pca[ix])
		# 	else:
		# 		img_fc7_list.append(np.zeros(np.shape(fc7_pca[0])))

		# # img_fc7_list = [fc7_pca[imid2ix[str(img) + ".jpg"]] for img in img_list]
		# if str(source_imid) + ".jpg" in imid2ix:
		key = "COCO_val2014_" + str(source_imid).zfill(12) + ".jpg"
		# print key
		source_fc7 = cocofeats_pca[cocoimid2ix[key]]
		# else:
		# 	return False
		# # Source fc7
		# print "Computing distances"
		# distances = [np.linalg.norm(img_fc7 - source_fc7) for img_fc7 in img_fc7_list]
		# img_tuples_list = zip(img_list, distances)

		# Find farthest fc7 neighbor
		# print "Sorting distances"
		# negative_img = sorted(img_tuples_list, key=lambda x: x[1])[-1][1]

		# Searching in 100k closest neighbors -- faster

		neighbors = index.get_nns_by_vector(source_fc7, 100000)
		neighbor_imids = [ix2imid[neighbor] for neighbor in neighbors]
		# print neighbor_imids[1:100]
		found = False
		count = 0
		for neighbor in neighbors:
			if int(ix2imid[neighbor].split(".")[0]) in img_list:
				negative_img = ix2imid[neighbor]
				count += 1
				if count > 4:
					found = True
					break
				key = int(negative_img.split(".")[0])
				if key in imid2obj:
					obj_anns = imid2obj[key]
				else:
					obj_anns = {}

				neg_img_list.append({
					"include": include,
					"exclude": exclude,
					"negative_img" + str(count): negative_img,
					"obj_anns" + str(count): obj_anns
					})
				
		if not found:
			print "Not found"
			return False

	return neg_img_list

def main():
	'''
	Find negative object images for each tuple
	'''
	rel2image = build_indexes()
	out = []

	with open("fp_obj_input.json", "r") as f:
		data = json.loads(f.read())
		for i in range(0, 100):
			qobj = data[i]
			neg_img_list = retrieve_neg_img_list(qobj["obj_list"], qobj["imid"])
			if neg_img_list:
				qobj["neg_img_list"] = neg_img_list
				out.append(qobj)
	# neg_img_list = retrieve_neg_img_list(["man", "car"], "53.jpg")
	# print neg_img_list

	with open("negative_object_output.json", "w") as f:
		json.dump(out, f, indent=4)

if __name__ == "__main__":					
	main()