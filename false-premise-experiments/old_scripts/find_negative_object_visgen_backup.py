import sys
import json
import pprint
import pickle
import torchfile
import numpy as np

# import gensim
from nltk.corpus import wordnet as wn
# from nltk.stem.wordnet import WordNetLemmatizer

from annoy import AnnoyIndex
sys.path.insert(0, './visual_genome_python_driver')

import src.local as vg

dataDir = "./VisualGenome/"
imageDataDir = "./data/by-id/"

# print "Loading word2vec..."
# w2v = gensim.models.Word2Vec.load_word2vec_format('../word2vec.torch/GoogleNews-vectors-negative300.bin', binary=True)  
# print "Loaded successfully"

N_TREES = 10
obj2imid, imid2obj = {}, {}
fc7_pca = {}
imid2ix, ix2imid, cocoimid2ix = {}, {}, {}
index = {}
fc7_vqa_pca = {}
cocofeats_pca = {}

def build_indexes():
	'''
	Build mapping from objects to image id's.
	'''
	print "Building Index..."
	# scene_graphs = vg.GetSceneGraphs(startIndex=1, endIndex=-1, minRels=1, \
 #                                 	 dataDir=dataDir, imageDataDir=imageDataDir)
 	global obj2imid
 	global fc7_pca
 	global imid2ix
 	global ix2imid
 	global index
 	global cocofeats_pca
 	global cocoimid2ix
 	global imid2obj

	# for ix, scene_graph in enumerate(scene_graphs):
	# 	obj_list = scene_graph.objects
	# 	for obj in obj_list:
	# 		for obj_synset in obj.synsets:
	# 			# print obj_synset_root
	# 			# obj_synset_aug = wn.synset(obj_synset_root).hypernyms()
	# 			# print obj_synset_aug
	# 			# obj_synset_aug.append(wn.synset(obj_synset_root))
	# 			# print obj_synset_aug
	# 			# for obj_synset in obj_synset_aug:
	# 			try:
	# 				if ((str(wn.synset(obj_synset)) in obj2imid.keys())):
	# 					if scene_graph.image.id not in obj2imid[str(wn.synset(obj_synset))]:
	# 						obj2imid[str(wn.synset(obj_synset))].append(scene_graph.image.id)
	# 				else:
	# 					obj2imid[str(wn.synset(obj_synset))] = [scene_graph.image.id]
	# 			except Exception as e:
	# 				print "Exception"
	# pickle.dump( obj2imid, open("obj2imid.p", "wb") )

	obj2imid_file = open("obj2imid.p", "rb")
	obj2imid = pickle.load(obj2imid_file)
	fc7_pca = np.load("visgenfeats_pca.npy")
	fc7_imid = torchfile.load("visgen/val_fc7_image_id.t7")

	cocolist = torchfile.load("../toaim/data/val_fc7_image_id.t7")
	cocofeats_pca = np.load("cocofeats_pca.npy")
	# print "Building NN index..."
	# index = AnnoyIndex(100, metric="euclidean")
	for ix, imid in enumerate(fc7_imid):
		imid2ix[imid] = ix
		ix2imid[ix] = imid
		# index.add_item(ix, fc7_pca[ix])

	# index.build(N_TREES)
	# index.save("index.ann")

	for ix, imid in enumerate(cocolist):
		cocoimid2ix[imid] = ix

	# print cocoimid2ix.keys()[1:10]
	index.load("index.ann")

	imid2obj = pickle.load(open("imid2obj.p", "rb"))
	print "All indexes built."

def resolve(obj):
	'''
	Return closest object class from Genome annotations to input string
	'''	
	# TODO: Add word sense disambiguation, alternatively CONSTRAIN the POS
	resolved = wn.synsets(obj.lower())
	for syn in resolved:
		# print syn
		if str(syn) in obj2imid:
			# print obj2imid[str(syn)]
			return str(syn)
	return False
	# print str(resolved[0])
	# return str(resolved[0]) if (len(resolved)>0 and (str(resolved[0]) in obj2imid)) else False # Assuming order is based on popularity of usage

def retrieve_neg_img_list(obj_list, source_imid):
	'''
	Return image with k-1, without kth for each k=1..len(obj_list),
	closest in fc7 to source.
	'''
	neg_img_list = []

	cococategories = "person, bicycle, car, motorcycle, airplane, bus, train, truck, boat, traffic light, fire hydrant, stop sign, parking meter, bench, bird, cat, dog, horse, sheep, cow, elephant, bear, zebra, giraffe, backpack, umbrella, handbag, tie, suitcase, frisbee, skis, snowboard, sports ball, kite, baseball bat, baseball glove, skateboard, surfboard, tennis racket, bottle, wine, glass, cup, fork, knife, spoon, bowl, banana, apple, sandwich, orange, broccoli, carrot, hot dog, pizza, donut, cake, chair, couch, potted plant, bed, dining table, toilet, tv, laptop, mouse, remote, keyboard, cell phone, microwave, oven, toaster, sink, refrigerator, book, clock, vase, scissors, teddy bear, hair, drier, toothbrush".split(",")
	cococategories = [category.strip() for category in cococategories]

	for ix, obj in enumerate(obj_list):
		exclude = obj
		print "Exclude", exclude
		if (exclude not in cococategories) or (not resolve(exclude)):
			continue

		exclude_imglist = obj2imid[resolve(exclude)]
		# print len(exclude_imglist)

		key = "COCO_val2014_" + str(source_imid).zfill(12) + ".jpg"
		source_fc7 = cocofeats_pca[cocoimid2ix[key]]

		neighbors = index.get_nns_by_vector(source_fc7, 10000)
		neighbor_imids = [ix2imid[neighbor] for neighbor in neighbors]

		found = False
		count = 0
		# print neighbor
		for ix, neighbor in enumerate(neighbor_imids):
			key = int(neighbor.split(".")[0])
			if key in imid2obj:
				obj_anns = imid2obj[key]
				# print resolve(exclude)
				# print "----------------------"
				# print obj_anns
				if resolve(exclude) not in obj_anns:
					count += 1
					if count > 4:
						found = True
						break
					neg_img_list.append({
						"exclude": exclude,
						"negative_img": neighbor,
						"obj_anns": obj_anns,
						"neighbor_pos": ix
						})		
		if not found:
			print "Not found"
			return False

		# s = raw_input('--> ')
	return neg_img_list

def main():
	'''
	Find negative object images for each tuple
	'''
	rel2image = build_indexes()
	out = []

	with open("fp_obj_input.json", "r") as f:
		data = json.loads(f.read())
		for i in range(0, 1000):
			qobj = data[i]
			neg_img_list = retrieve_neg_img_list(qobj["obj_list"], qobj["imid"])
			if neg_img_list:
				qobj["neg_img_list"] = neg_img_list
				out.append(qobj)
	# neg_img_list = retrieve_neg_img_list(["man", "car"], "53.jpg")
	# print neg_img_list

	with open("negative_object_output.json", "w") as f:
		json.dump(out, f, indent=4)

if __name__ == "__main__":					
	main()