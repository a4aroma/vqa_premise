import os
import cPickle

import parse_genome as parse_genome

def retrieve():
	pass

def main():
	cwd = os.getcwd()
	input_data_path = cwd
	store_path = input_data_path + "/store"

	# sg = parse_genome.parse()
	# parse_genome.build_rel2image(sg, store_path)

	rel2image = cPickle.load(open(store_path + "/rel2image.p", "rb"))

	while True:
		ip_rel = raw_input('Lookup images for --> ')
		if ip_rel in rel2image:
			print("Images found: ", rel2image[ip_rel])
		else:
			print("Relation not found in index.")

if __name__ == "__main__":					
	main()
