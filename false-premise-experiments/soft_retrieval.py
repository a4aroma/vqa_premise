import sys
import nltk
import json
import string
import cPickle
import numpy as np
import os

from annoy import AnnoyIndex

import utils.w2v as w2v_helpers

def compute_score(caption, w2v):
	words = parse_sen(caption)
	count = 0
	caption_score = np.zeros(300)
	for ix, word in enumerate(words):
		if word in w2v:
			caption_score += w2v[word]
			count += 1
	if count == 0:
		return caption_score
	return caption_score / float(count)

def parse_sen(sentence):
	valid_chars = string.ascii_letters + string.digits + ' 	'
	sentence = ''.join(c for c in sentence if c in valid_chars)
	tagged_sentence = nltk.pos_tag(nltk.word_tokenize(sentence))
	sen_wordlist = []
	for item in tagged_sentence:
		# if item[1][0] == 'N' or item[1][0] == 'V' or item[1][0] == 'J' or item[1][:2] == 'RB':
		sen_wordlist.append(item[0])
	result = sen_wordlist
	return result

def build_relationship_index(w2v, input_path, store_path):
	t = AnnoyIndex(300, metric="euclidean")
	caption_list = []
	with open(input_path + "/rel_counts.json", "r") as f:
		rel_tups = json.loads(f.read())
		skipped = 0
		for ix, tup in enumerate(rel_tups):
			caption = " ".join([tup[0].split("_")[0], tup[0].split("_")[2]])
			score = compute_score(caption, w2v)
			if not np.any(score):
				skipped += 1
			else:
				t.add_item(ix-skipped, score)
				caption_list.append(tup[0])

		print("Skipped ", skipped)
		t.build(10)
		t.save(store_path + "/rel_index.ann")
		cPickle.dump(caption_list, open(store_path + "/rel_index.p", "wb"))
		return t, caption_list

def similarity_score(target_lemma, pred_lemma, w2v):
	# Higher similarity score = LESS similarity
	target_score = compute_score(target_lemma, w2v)
	pred_score = compute_score(pred_lemma, w2v)
	if (not np.any(target_score)) or (not np.any(pred_score)):
		return sys.maxint

	return np.linalg.norm(target_score - pred_score)

def main():
	w2v = w2v_helpers.load_model()

	cwd = os.getcwd()
	input_data_path = cwd
	store_path = input_data_path + "/store"

	t = AnnoyIndex(300, metric="euclidean")
	t.load(store_path + "/rel_index.ann")

	caption_list = cPickle.load(open(store_path + "/rel_index.p", "rb"))
	
	# t, caption_list = build_relationship_index(w2v, input_data_path, store_path)

	while True:
		ip_caption = raw_input('Lookup relationships similar to --> ')
		score = compute_score(ip_caption, w2v)
		nns = t.get_nns_by_vector(score, 20)
		closest_captions = [caption_list[i] for i in nns]
		print(closest_captions)
		ip_pair = raw_input('Lookup similarity between --> ')
		print(similarity_score(ip_pair.split(" ")[0], ip_pair.split(" ")[1:], w2v))

if __name__ == "__main__":					
	main()




