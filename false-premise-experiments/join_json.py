import sys
import json
from os import listdir
from os.path import isfile, join

def join_files(mypath, outfile):
	onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
	all_data = []
	for f in onlyfiles:
		with open(join(mypath, f), 'r') as ffile:
			jdata = json.load(ffile)
		all_data.extend(jdata)
	with open(outfile, 'w') as ofile:
		json.dump(all_data, ofile)

def main():
	args = sys.argv[1:]
	join_files(args[0], args[1])

if __name__ == "__main__":
	main()
