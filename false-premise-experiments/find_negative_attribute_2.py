import sys
import os
import os.path
import json
import pickle
import multiprocessing

sys.path.insert(0, './visual_genome_python_driver')

import src.local as vg
import gensim
import nltk
from nltk.tag import StanfordPOSTagger
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer

import torchfile
from sklearn.metrics.pairwise import cosine_similarity
from scipy import sparse

os.environ['STANFORD_MODELS'] = '/home/aroma/stanfordnlp/stanford-postagger'
path_to_model = '/home/aroma/stanfordnlp/stanford-postagger/models/english-bidirectional-distsim.tagger'
path_to_jar = '/home/aroma/stanfordnlp/stanford-postagger/stanford-postagger.jar'

dataDir = "/srv/share/data/VisualGenome_v1.2/"
vgDir = "/srv/share/data/VisualGenome_v1.2/scene_graphs.json"
imageDataDir = "by-id/"
storedIndex = "verb_idx.json"
inputFile = "attr_data.json"
outFile = "attr_dataset_v3.json"

with open('antonyms-and-others.pkl','r') as afile:
	[antonyms, colors, locations] = pickle.load(afile)

# vg.AddAttrsToSceneGraphs(dataDir=dataDir)
# vg.SaveSceneGraphsById(dataDir=dataDir, imageDataDir=imageDataDir)

#TODO: move distance calculation to another script

print "Loading word2vec..."
w2v = gensim.models.Word2Vec.load_word2vec_format('/home/aroma/GoogleNews-vectors-negative300.bin', binary=True)  
print "Loaded successfully"


def build_indexes():
	if os.path.isfile(storedIndex):
		with open(storedIndex, "r") as index_file:
			attr_image = json.load(index_file)
		print "Index loaded .."
	else:
		print "Index not found, building Index..."
		attr_image = {}
		#pool = multiprocessing.Pool(16)
		#scene_graphs = vg.GetSceneGraphs(startIndex=0, endIndex=-1, minRels=1, \
	    #                             dataDir=dataDir, imageDataDir=imageDataDir)
		with open(vgDir,"r") as sgfile:
			scene_graphs = json.load(sgfile)
		for ix, scene_graph in enumerate(scene_graphs):
			if ix%10 == 0:
				print ix
			for sg_att_ob in scene_graph["attributes"]:
				sg_att = sg_att_ob["attribute"]
				if "attributes" in sg_att:
					for att in sg_att["attributes"]:
						key = sg_att["names"][0]
						node_2 = nltk.pos_tag([att])[0][1]
						tag = list(node_2)
						obj = {
							"image_id": sg_att_ob['image_id'],
							"attr": att 
						}
						if key in attr_image and tag[0] == 'V':
							l = len(attr_image[key])
							if attr_image[key][l-1] != obj:
								attr_image[key].append(obj)
						elif tag[0] == 'V':
							attr_image[key] = [obj]
				else:
					pass
		print "Index built"
		with open(storedIndex, "w") as f:
		 	json.dump(attr_image, f)

	return attr_image


def GetSimilarity(q, sg_obj, dtype='w2v'):
	pred = q['tuple'][1]
	target_pred = sg_obj["attr"]
	if pred != target_pred:
		if pred in w2v and target_pred in w2v \
		and target_pred not in ['doing','have','having','look']:
		#if pred in w2v and target_pred in w2v:
			w2vsim = w2v.similarity(pred,target_pred)
			w2vsim = 0.0 if w2vsim > 0.3 else w2vsim
			return(w2vsim)
		else:
			return(0.0)
	else:
		return(0.0)




def main():
	args = sys.argv[1:]
	'''
	Find negative images for this tuple
	'''
	attr_image = build_indexes()
	out = []
	with open(args[0], "r") as f:
		data = json.load(f)
		it = 0
		print len(data)
		for q in data:
		#for i in range(2):
			#q = data[i]
			if it % 50 == 0:
				print it
			it = it + 1
			target_key = q["tuple"][0]
			target_pred = q["tuple"][1]
			q_nb = True if q['question'].split()[0].lower() not in ['is','are','can','does','would','could'] else False
			if target_pred not in ['doing','have','having','look'] :
				if target_key in attr_image and\
				target_key not in ['one','thing','corner', 'enough','there','it','object','piece', 'section','letter' \
				'that','item','word','time','part','end','structure','level','name','stuff','body'] \
				and q_nb and target_key not in locations:
					scene_graph_objs = attr_image[target_key]
					curr_max, max_id = -1, -1	
					for ix, scene_graph_obj in enumerate(scene_graph_objs):
						pred = scene_graph_obj["attr"]
						image_id = scene_graph_obj["image_id"]
						sim = GetSimilarity(q,scene_graph_obj)
						#print 'get sim done'
						if sim > curr_max:
							curr_max = sim
							max_id = ix
					#print 'loop i done'
					if max_id != -1:
						out.append({
							"q": q["question"],
							"rel_imid": q["image_id"],
							"irr_imid": scene_graph_objs[max_id]["image_id"],
							"rel_tuple": "_".join(q["tuple"]),
							"irr_tuple": q["tuple"][0]+"_"+scene_graph_objs[max_id]["attr"],
							"sim": curr_max
							})
		#print 'loop o done'			
		out.sort(key=lambda x: x["sim"], reverse=False)
			
		with open(args[1], "w") as f:
			json.dump(out, f, indent=4)

if __name__ == "__main__":					
	main()
	#build_indexes()