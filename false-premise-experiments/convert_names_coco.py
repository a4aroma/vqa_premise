import sys
import json
import torchfile

def convert_names_coco(input_file, output_file):
	with open(input_file,'r') as afile:
		adata = json.load(afile)

	coco_train = torchfile.load('train_fc7_image_id.t7')

	for a  in adata:
		for c in coco_train:
			c_int  = int(c.split('_')[2].split('.')[0])
			if c_int == a['rel_imid']:
				a['rel_name'] = c
				break
		for c in coco_train:
			c_int  = int(c.split('_')[2].split('.')[0])
			if c_int == a['irr_imid']:
				a['irr_name'] = c
				break

	with open(output_file,'w') as afile:
		json.dump(adata,afile)

def main():
	args = sys.argv[1:]
	convert_names_coco(args[0], args[1])

if __name__ == "__main__":
	main()