import sys
import json
import torchfile
import numpy as np

def compute_acc(tfile):
	tfile = torchfile.load(tfile)
	scores = tfile['scores']
	labels = tfile['labels']
	qids = tfile['qid']
	#with open('att_ids.json') as afile:
	#	att_ids = json.load(afile)
	#with open('comm_ids.json') as cfile:
	#	comm_ids = json.load(cfile)

	#qids = [str(q) for q in qids]
	#att_ids = [str(a) for a in att_ids]
	#comm_ids = [str(c) for c in comm_ids]

	preds = np.argmax(scores, axis=1)
	preds += 1
	acc = float((preds == labels).sum())/float(len(labels))
	#print((preds == 1).sum())
	#print((preds == 2).sum())
	#att_count = 2*len(att_ids)
	#ob_count = len(labels) -  att_count + 2*len(comm_ids)
	#att_run = 0
	#ob_run = 0
	#for i in range(len(labels)):
	#	if preds[i] == labels[i]:
	#		if qids[i] in att_ids:
	#			att_run = att_run + 1
	#
	#		elif qids[i] in comm_ids:
	#			att_run = att_run + 1
	#			ob_run = ob_run + 1
	#		else:
	#			ob_run = ob_run + 1
	#
	#att_acc = float(att_run)/float(att_count)
	#ob_acc = float(ob_run)/float(ob_count)
	return acc

def main():
	args = sys.argv[1:]
	acc = compute_acc(args[0])
	print('val acc : '+str(acc))
	#print('attr acc: '+str(att_acc))
	#print('ob acc: '+str(ob_acc))

if __name__ == '__main__':
	main()