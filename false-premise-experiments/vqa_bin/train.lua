require 'nn'
require 'torch'
require 'nngraph'
require 'optim'
require 'misc.netdef'
require 'cutorch'
require 'cunn'
require 'hdf5'
cjson=require('cjson') 
LSTM=require 'misc.LSTM'

-------------------------------------------------------------------------------
-- Input arguments and options
-------------------------------------------------------------------------------

cmd = torch.CmdLine()
cmd:text()
cmd:text('Train a Visual Question Answering model')
cmd:text()
cmd:text('Options')

-- Data input settings
cmd:option('-input_img_coco','../train_fc7.t7','path to the h5file containing the image feature')
cmd:option('-input_img_coco_val','../val_fc7.t7','path to the h5file containing the image feature')
cmd:option('-input_img_vg','../vg_fc7.t7','path to the h5file containing the image feature')
cmd:option('-input_ques_h5','vqa_bin.h5','path to the h5file containing the image feature')
--cmd:option('-input_ques_h5','data_prepro.h5','path to the h5file containing the preprocessed dataset')
cmd:option('-input_json','vqa_bin.json','path to the json file containing additional info and vocab')
cmd:option('-model_path', 'model/', 'path to a model checkpoint to initialize model weights from. Empty = don\'t')
cmd:option('-lambda',0.001,'lambda parameter for L1 Penalty')
-- Model parameter settings
cmd:option('-learning_rate',3e-4,'learning rate for rmsprop')
cmd:option('-learning_rate_decay_start', -1, 'at what iteration to start decaying learning rate? (-1 = dont)')
cmd:option('-learning_rate_decay_every', 50000, 'every how many iterations thereafter to drop LR by half?')
cmd:option('-batch_size',500,'batch_size for each iterations')
cmd:option('-max_iters', 150000, 'max number of iterations to run for ')
cmd:option('-input_encoding_size', 200, 'he encoding size of each token in the vocabulary')
cmd:option('-rnn_size',512,'size of the rnn in number of hidden nodes in each layer')
cmd:option('-rnn_layer',2,'number of the rnn layer')
cmd:option('-common_embedding_size', 1024, 'size of the common embedding vector')
cmd:option('-num_output', 2, 'number of output answers')
cmd:option('-img_norm', 0, 'normalize the image feature. 1 = normalize, 0 = not normalize')

--check point
cmd:option('-save_checkpoint_every', 25000, 'how often to save a model checkpoint?')
cmd:option('-checkpoint_path', 'model/', 'folder to save checkpoints')

-- misc
cmd:option('-backend', 'cudnn', 'nn|cudnn')
cmd:option('-gpuid', 0, 'which gpu to use. -1 = use CPU')
cmd:option('-seed', 123, 'random number generator seed to use')

opt = cmd:parse(arg)
print(opt)

torch.manualSeed(opt.seed)
torch.setdefaulttensortype('torch.FloatTensor') -- for CPU

require 'misc.RNNUtils'
if opt.gpuid >= 0 then
  require 'cutorch'
  require 'cunn'
  if opt.backend == 'cudnn' then require 'cudnn' end
  cutorch.manualSeed(opt.seed)
  cutorch.setDevice(opt.gpuid + 1)
end

------------------------------------------------------------------------
-- Setting the parameters
------------------------------------------------------------------------

local model_path = opt.model_path
local batch_size=opt.batch_size
local embedding_size_q=opt.input_encoding_size
local lstm_size_q=opt.rnn_size
local nlstm_layers_q=opt.rnn_layer
local nhimage=4096
local common_embedding_size=opt.common_embedding_size
local noutput=opt.num_output
local dummy_output_size=1
local l1_mult = opt.lambda
local decay_factor = 0.99997592083 -- 50000
paths.mkdir(model_path)

------------------------------------------------------------------------
-- Loading Dataset
------------------------------------------------------------------------
local file = io.open(opt.input_json, 'r')
local text = file:read()
file:close()
json_file = cjson.decode(text)

local file = io.open('../coco_train_dict.json', 'r')
local text = file:read()
file:close()
coco_train_dict = cjson.decode(text)

local file = io.open('../coco_val_dict.json', 'r')
local text = file:read()
file:close()
coco_val_dict = cjson.decode(text)

local file = io.open('../vg_dict.json', 'r')
local text = file:read()
file:close()
vg_dict = cjson.decode(text)

print('DataLoader loading h5 file: ', opt.input_ques_h5)
local dataset = {}
local h5_file = hdf5.open(opt.input_ques_h5, 'r')

dataset['question'] = h5_file:read('/ques_train'):all()
dataset['lengths_q'] = h5_file:read('/ques_length_train'):all()
dataset['labels'] = h5_file:read('/labels_train'):all()
dataset['ttype'] = h5_file:read('/ttype_train'):all()
--print(dataset['question']:size())
dataset['question_test'] = h5_file:read('/ques_test'):all()
dataset['lengths_q_test'] = h5_file:read('/ques_length_test'):all()
dataset['labels_test'] = h5_file:read('/labels_test'):all()
dataset['ttype_test'] = h5_file:read('/ttype_test'):all()
h5_file:close()

dataset['imid_train'] = json_file['imid_train']
dataset['imid_test'] = json_file['imid_test']

print('DataLoader loading t7 file: ', opt.input_img_coco)
dataset['fv_coco'] = torch.load(opt.input_img_coco)

print('DataLoader loading t7 file: ', opt.input_img_coco_val)
dataset['fv_coco_val'] = torch.load(opt.input_img_coco_val)

print('DataLoader loading t7 file: ', opt.input_img_vg)
dataset['fv_vg'] = torch.load(opt.input_img_vg)

dataset['question'] = right_align(dataset['question'],dataset['lengths_q'])
dataset['question_test'] = right_align(dataset['question_test'],dataset['lengths_q_test'])
--print(dataset['question']:size())
-- Normalize the image feature
if opt.img_norm == 1 then
	local nm=torch.sqrt(torch.sum(torch.cmul(dataset['fv_coco'],dataset['fv_coco']),2)) 
	dataset['fv_coco']=torch.cdiv(dataset['fv_coco'],torch.repeatTensor(nm,1,4096)):float() 
	local nm=torch.sqrt(torch.sum(torch.cmul(dataset['fv_coco_val'],dataset['fv_coco_val']),2)) 
	dataset['fv_coco_val']=torch.cdiv(dataset['fv_coco_val'],torch.repeatTensor(nm,1,4096)):float()
	local nm=torch.sqrt(torch.sum(torch.cmul(dataset['fv_vg'],dataset['fv_vg']),2)) 
	dataset['fv_vg']=torch.cdiv(dataset['fv_vg'],torch.repeatTensor(nm,1,4096)):float()
else
	dataset['fv_coco']=dataset['fv_coco']:float()
	dataset['fv_coco_val']=dataset['fv_coco_val']:float()
	dataset['fv_vg']=dataset['fv_vg']:float()
end

local count = 0
for i, w in pairs(json_file['ix_to_word']) do count = count + 1 end
local vocabulary_size_q=count

collectgarbage() 

------------------------------------------------------------------------
--Design Parameters and Network Definitions
------------------------------------------------------------------------
print('Building the model...')

buffer_size_q=dataset['question']:size()[2]

--Network definitions
--VQA
--embedding: word-embedding
embedding_net_q=nn.Sequential()
				:add(nn.Linear(vocabulary_size_q,embedding_size_q))
				:add(nn.Dropout(0.5))
				:add(nn.Tanh())
				:add(nn.L1Penalty(l1_mult))

--encoder: RNN body
encoder_net_q=LSTM.lstm_conventional(embedding_size_q,lstm_size_q,dummy_output_size,nlstm_layers_q,0.5)

--MULTIMODAL
--multimodal way of combining different spaces
shared_net = nn.Sequential()
			 :add(netdef.AxB(2*lstm_size_q*nlstm_layers_q,nhimage,common_embedding_size,0.5))
			 :add(nn.Dropout(0.5))
layer_2way = nn.Sequential()
			 :add(nn.Linear(common_embedding_size, noutput))

multimodal_net=nn.Sequential()
			   :add(shared_net)
			   :add(nn.Linear(common_embedding_size, 1000))
multimodal_net_ft = nn.Sequential()
					:add(shared_net)
					:add(layer_2way)
					:add(nn.L1Penalty(l1_mult))

--criterion
criterion=nn.CrossEntropyCriterion()

--Optimization parameters
dummy_state_q=torch.Tensor(lstm_size_q*nlstm_layers_q*2):fill(0)
dummy_output_q=torch.Tensor(dummy_output_size):fill(0)

if opt.gpuid >= 0 then
	print('shipped data function to cuda...')
	embedding_net_q = embedding_net_q:cuda()
	encoder_net_q = encoder_net_q:cuda()
	multimodal_net = multimodal_net:cuda()
	multimodal_net_ft = multimodal_net_ft:cuda()
	criterion = criterion:cuda()
	dummy_state_q = dummy_state_q:cuda()
	dummy_output_q = dummy_output_q:cuda()
end

--Processings
embedding_w_q,embedding_dw_q=embedding_net_q:getParameters()
--embedding_w_q:uniform(-0.08, 0.08)
encoder_w_q,encoder_dw_q=encoder_net_q:getParameters()
multimodal_w,multimodal_dw=multimodal_net:getParameters() 
layer_2way_w, layer_2way_dw = layer_2way:getParameters()
layer_2way_w:uniform(-0.08,0.08)
multimodal_ft_w,multimodal_ft_dw=multimodal_net_ft:getParameters()

sizes={encoder_w_q:size(1),embedding_w_q:size(1), multimodal_ft_w:size(1)} 

model_param=torch.load(model_path..'lstm.t7');
embedding_w_q:copy(model_param['embedding_w_q']);
encoder_w_q:copy(model_param['encoder_w_q']);
multimodal_w:copy(model_param['multimodal_w']);

-- optimization parameter
local optimize={} 
optimize.maxIter=opt.max_iters 
optimize.learningRate=opt.learning_rate
optimize.update_grad_per_n_batches=1 

optimize.winit=join_vector({encoder_w_q,embedding_w_q, multimodal_ft_w}) 


------------------------------------------------------------------------
-- Next batch for train
------------------------------------------------------------------------
function dataset:next_batch()
	local nqs=dataset['question']:size(1)
	local qinds=torch.LongTensor(batch_size):fill(0) 
	local iminds=torch.LongTensor(batch_size):fill(0) 	
	
	local fv_im = torch.LongTensor(batch_size, dataset['fv_coco']:size()[2]):fill(0)
	-- we use the last val_num data for validation (the data already randomlized when created)

	for i=1,batch_size do
		qinds[i]=torch.random(nqs) 
		if dataset['ttype'][qinds[i]] == 1 then
			fv_im[i] = dataset['fv_coco'][coco_train_dict[dataset['imid_train'][qinds[i]]]+1]
		else
			fv_im[i] = dataset['fv_vg'][vg_dict[dataset['imid_train'][qinds[i]]]+1]
		end
	end


	local fv_sorted_q=sort_encoding_onehot_right_align(dataset['question']:index(1,qinds),dataset['lengths_q']:index(1,qinds),vocabulary_size_q) 

	local labels=dataset['labels']:index(1,qinds) 
	
	-- ship to gpu
	if opt.gpuid >= 0 then
		fv_sorted_q[1]=fv_sorted_q[1]:cuda() 
		fv_sorted_q[3]=fv_sorted_q[3]:cuda() 
		fv_sorted_q[4]=fv_sorted_q[4]:cuda() 
		fv_im = fv_im:cuda()
		labels = labels:cuda()
	end

	return fv_sorted_q,fv_im, labels ,batch_size 
end

function dataset:construct_val()
	local nqs=dataset['question_test']:size(1)
	local qinds=torch.LongTensor(nqs):fill(0) 
	local iminds=torch.LongTensor(nqs):fill(0) 	
	
	local fv_im = torch.LongTensor(nqs, dataset['fv_coco_val']:size()[2]):fill(0)
	-- we use the last val_num data for validation (the data already randomlized when created)

	for i=1,nqs do
		if dataset['ttype_test'][i] == 1 then
			fv_im[i] = dataset['fv_coco_val'][coco_val_dict[dataset['imid_test'][i]]+1]
		else
			fv_im[i] = dataset['fv_vg'][vg_dict[dataset['imid_test'][i]]+1]
		end
	end


	local fv_sorted_q=sort_encoding_onehot_right_align(dataset['question_test'],dataset['lengths_q_test'],vocabulary_size_q) 

	local labels=dataset['labels_test'] 
	
	-- ship to gpu
	if opt.gpuid >= 0 then
		fv_sorted_q[1]=fv_sorted_q[1]:cuda() 
		fv_sorted_q[3]=fv_sorted_q[3]:cuda() 
		fv_sorted_q[4]=fv_sorted_q[4]:cuda() 
		fv_im = fv_im:cuda()
		labels = labels:cuda()
	end

	return fv_sorted_q, fv_im, labels , nqs
end

------------------------------------------------------------------------
-- Objective Function and Optimization
------------------------------------------------------------------------

-- duplicate the RNN
local encoder_net_buffer_q=dupe_rnn(encoder_net_q,buffer_size_q) 

-- Objective function
function JdJ(x)
	local params=split_vector(x,sizes) 
	--load x to net parameters--
	if encoder_w_q~=params[1] then
		encoder_w_q:copy(params[1]) 
		for i=1,buffer_size_q do
			encoder_net_buffer_q[2][i]:copy(params[1]) 
		end
	end
	if embedding_w_q~=params[2] then
		embedding_w_q:copy(params[2]) 
	end
	--if multimodal_w~=params[3] then
	--	multimodal_w:copy(params[3]) 
	--end
	if multimodal_ft_w~=params[3] then
		multimodal_ft_w:copy(params[3]) 
	end

	--clear gradients--
	for i=1,buffer_size_q do
		encoder_net_buffer_q[3][i]:zero() 
	end
	embedding_dw_q:zero() 
	--multimodal_dw:zero() 
	multimodal_ft_dw:zero()

	--grab a batch--
	local fv_sorted_q,fv_im,labels,batch_size=dataset:next_batch() 
	local question_max_length=fv_sorted_q[2]:size(1) 

	--embedding forward--
	local word_embedding_q=split_vector(embedding_net_q:forward(fv_sorted_q[1]),fv_sorted_q[2]) 

	--encoder forward--
	local states_q,junk2=rnn_forward(encoder_net_buffer_q,torch.repeatTensor(dummy_state_q:fill(0),batch_size,1),word_embedding_q,fv_sorted_q[2]) 
	
	--multimodal/criterion forward--
	local tv_q=states_q[question_max_length+1]:index(1,fv_sorted_q[4]) 
	local scores=multimodal_net_ft:forward({tv_q,fv_im})
	--local scores = multimodal_net_ft:forward(scores_1k) 
	local f=criterion:forward(scores,labels) 
	--multimodal/criterion backward--
	local dscores=criterion:backward(scores,labels) 
	--local dscores1k = multimodal_net_ft:backward(scores_1k, dscores)
	local tmp=multimodal_net_ft:backward({tv_q,fv_im},dscores) 
	local dtv_q=tmp[1]:index(1,fv_sorted_q[3]) 
	--encoder backward
	local junk4,dword_embedding_q=rnn_backward(encoder_net_buffer_q,dtv_q,dummy_output_q,states_q,word_embedding_q,fv_sorted_q[2]) 

	--embedding backward--
	dword_embedding_q=join_vector(dword_embedding_q) 
	embedding_net_q:backward(fv_sorted_q[1],dword_embedding_q) 
		
	--summarize f and gradient
	local encoder_adw_q=encoder_dw_q:clone():zero()
	for i=1,question_max_length do
		encoder_adw_q=encoder_adw_q+encoder_net_buffer_q[3][i] 
	end
	gradients=join_vector({encoder_adw_q,embedding_dw_q,multimodal_ft_dw}) 
	gradients:clamp(-10,10) 
	if running_avg == nil then
		running_avg = f
	end
	running_avg=running_avg*0.95+f*0.05 
	return f,gradients 
end

function compute_val_loss()
	local fv_sorted_q,fv_im,labels,batch_size=dataset:construct_val() 
	local question_max_length=fv_sorted_q[2]:size(1) 

	--embedding forward--
	local word_embedding_q=split_vector(embedding_net_q:forward(fv_sorted_q[1]),fv_sorted_q[2]) 

	--encoder forward--
	local states_q,junk2=rnn_forward(encoder_net_buffer_q,torch.repeatTensor(dummy_state_q:fill(0),batch_size,1),word_embedding_q,fv_sorted_q[2]) 
	
	--multimodal/criterion forward--
	local tv_q=states_q[question_max_length+1]:index(1,fv_sorted_q[4]) 
	local scores=multimodal_net_ft:forward({tv_q,fv_im}) 
	--local scores = multimodal_net_ft:forward(scores_1k)
	local f=criterion:forward(scores,labels) 

	return f, scores

end
----------------------------------------------------------------------------------------------
-- Training
----------------------------------------------------------------------------------------------
-- With current setting, the network seems never overfitting, so we just use all the data to train

local state={}
for iter = 1, opt.max_iters do
	if iter%opt.save_checkpoint_every == 0 then
		paths.mkdir(model_path..'save')
		torch.save(string.format(model_path..'save/lstm_save_iter%d.t7',iter),
			{encoder_w_q=encoder_w_q,embedding_w_q=embedding_w_q, multimodal_ft_w= multimodal_ft_w}) 
		--val_loss, val_scores = compute_val_loss()
		--print('val loss: ' .. val_loss, 'on iter: ' .. iter .. '/' .. opt.max_iters)
		--torch.save(string.format(model_path..'save/preds/preds%d.t7',iter),
		--	{scores = val_scores, labels = labels_test})
	end
	if iter%100 == 0 then
		print('training loss: ' .. running_avg, 'on iter: ' .. iter .. '/' .. opt.max_iters)
	end
	optim.rmsprop(JdJ, optimize.winit, optimize, state)
	
	optimize.learningRate=optimize.learningRate*decay_factor 
	if iter%50 == 0 then -- change this to smaller value if out of the memory
		collectgarbage()
	end
end

-- Saving the final model
--torch.save(string.format(model_path..'lstm.t7',i),
--	{encoder_w_q=encoder_w_q,embedding_w_q=embedding_w_q,multimodal_w=multimodal_w}) 
