import sys
import json
import torchfile

def convert_names(input_file, output_file):
	with open(input_file,'r') as afile:
		adata = json.load(afile)

	coco_train = torchfile.load('train_fc7_image_id.t7')

	for a  in adata:
		a['vg_name'] = str(a['irr_imid'])+'.jpg'
		for c in coco_train:
			c_int  = int(c.split('_')[2].split('.')[0])
			if c_int == a['rel_imid']:
				a['coco_name'] = c
				break

	with open(output_file,'w') as afile:
		json.dump(adata,afile)

def main():
	args = sys.argv[1:]
	convert_names(args[0], args[1])

if __name__ == "__main__":
	main()
