import torchfile
import numpy as np
from sklearn.decomposition import PCA

vqatrainfeats = torchfile.load("/srv/share/vqa_premise/fc7_features/train_fc7.t7")
visgenfeats = torchfile.load("/home/virajp/code/false-premise-experiments/visgen/val_fc7.t7")

allfeats = np.vstack((vqatrainfeats, visgenfeats))

pca = PCA(n_components=100)
allfeats_pca = pca.fit_transform(allfeats)

vqatrainfeats_pca = allfeats_pca[:len(vqatrainfeats)]
visgenfeats_pca = allfeats_pca[len(vqatrainfeats):]

np.save("vqatrainfeats_pca.npy", vqatrainfeats_pca)
np.save("visgenfeats_pca.npy", visgenfeats_pca)