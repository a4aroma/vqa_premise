import json

def parse(data):
	qid2ixlist = {}
	for ix, val in enumerate(data):
		if val["qid"] in qid2ixlist:
			qid2ixlist[val["qid"]].append(ix)
		else:
			qid2ixlist[val["qid"]] = [ix]
	parsed = []
	for k,v in qid2ixlist.iteritems():
		tuplist = []
		for pos in v:
			tupobj = data[pos]
			tuplist.append({
				"rel_tuple": tupobj["rel_tuple"],
				"irr_tuple": tupobj["irr_tuple"],
				"rel_imid": tupobj["rel_imid"],
				"irr_imid": tupobj["irr_imid"]
				})

		parsed.append({
			"q": data[v[0]]["q"],
			"qid": data[v[0]]["qid"],
			"tuplist": tuplist
			})
	return parsed

def create_splits(data):
	ix = int(len(data)*2/3)
	return data[:ix], data[ix:]

def main():
	with open("train/tups/objects_train_tup.json") as f1, open("train/tups/attributes_train_tup.json") as f2, \
		 open("test/tups/objects_test_tup.json") as f3, open("test/tups/attributes_test_tup.json") as f4:
		otrain = json.load(f1)
		otest = json.load(f3)

		atrain = json.load(f2)
		atest = json.load(f4)
		
		objects_train_new, objects_test_new = parse(otrain), \
			 							   	  parse(otest),

		objects_test_train_new, objects_test_test_new = create_splits(objects_test_new)
		
		attributes_train_new, attributes_test_new = parse(atrain), \
			 							   	  		parse(atest),

		attributes_test_train_new, attributes_test_test_new = create_splits(attributes_test_new)
	
		train = otrain + atrain
		test = otest + atest

		train_new = parse(train)
		test_new = parse(test)

		test_train_new, test_test_new = create_splits(test_new)

	with open("train/objects_train.json", "w") as f1, open("train/attributes_train.json", "w") as f2, \
		 open("test/objects_test_trainsplit.json", "w") as f3, open("test/objects_test_testsplit.json", "w") as f4, \
		 open("test/objects_test.json", "w") as f5, open("test/attributes_test.json", "w") as f6, \
		 open("test/attributes_test_trainsplit.json", "w") as f7, open("test/attributes_test_testsplit.json", "w") as f8, \
		 open("train/train.json", "w") as f9, open("test/test.json", "w") as f10, \
		 open("train/test_trainsplit.json", "w") as f11, open("test/test_testsplit.json", "w") as f12:
		
		json.dump(objects_train_new, f1, indent=4)
		json.dump(objects_test_new, f5, indent=4)
		json.dump(objects_test_train_new, f3, indent=4)
		json.dump(objects_test_test_new, f4, indent=4)

		json.dump(attributes_train_new, f2, indent=4)
		json.dump(attributes_test_new, f6, indent=4)
		json.dump(attributes_test_train_new, f7, indent=4)
		json.dump(attributes_test_test_new, f8, indent=4)
		
		json.dump(train_new, f9, indent=4)
		json.dump(test_new, f10, indent=4)
		
		json.dump(test_train_new, f11, indent=4)
		json.dump(test_test_new, f12, indent=4)

if __name__ == "__main__":
	main()