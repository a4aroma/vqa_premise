#!/bin/bash
gpucount=0

if [ $# -eq 0 ]; then
  # If no count passed in, assume 1
  gpucount=1
else
  gpucount=$1
fi

gpus=''
count=0

while [ $count -ne $gpucount ]; do
  gpus="$count,$gpus"
  count=$((count+1))
done

gpus=${gpus::-1}
echo $gpus
CUDA_VISIBLE_DEVICES=$gpus th extract_fc7.lua -gpuid $gpucount -input_image_dir "/srv/share/data/VisualGenome/images/"