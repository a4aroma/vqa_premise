"""
Script to extract word2vec features
"""
import gensim
import numpy as np

def load_model(model_path="/Users/virajprabhu/Documents/vt/vqaprem/code/word2vec.torch/GoogleNews-vectors-negative300.bin"):
	print "Loading word2vec..."
	model = gensim.models.Word2Vec.load_word2vec_format(model_path, binary=True)  
	print "Loaded successfully"
	return model

def wordfeat(word, model):
	word = word.encode("utf8")
	if word in model.vocab:
		feature = model[word]
	else:
		feature = np.zeros((300,), dtype="float32")
	return feature
