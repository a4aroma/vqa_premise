import json
import cPickle
import string

def filter_word(word):
	valid_chars = string.ascii_letters + string.digits + ' 	'
	word = ''.join(c for c in word if c in valid_chars)
	return word

with open("../input_data/visgen/relationships.json", "r") as f:
	relationships = json.loads(f.read())
	rel2imids = {}
	for ix, imgobj in enumerate(relationships):
		imid = imgobj["image_id"]
		for rel in imgobj["relationships"]:
			# if (len(rel["subject"]["synsets"]) > 0) and \
			#    (len(rel["object"]["synsets"]) > 0) and \
			#    (len(rel["synsets"]) > 0):
			   	
		   	rsubj = filter_word(rel["subject"]["name"])
		   	robj = filter_word(rel["object"]["name"])
		   	rpred = filter_word(rel["predicate"])

		   	rname = rsubj + "_" + rpred + "_" + robj

		   	if rname in rel2imids:
		   		rel2imids[rname].append(imid)
		   	else:
		   		rel2imids[rname] = [imid]

	cPickle.dump(rel2imids, open("../input_data/rel2imids.p", "wb"))