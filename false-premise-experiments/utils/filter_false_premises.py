import json

# Input the VQA question types file
with open("input_data/vqa/mscoco_question_types.txt", "r") as f:
    types = f.read().splitlines()
    types.sort(lambda x, y: cmp(len(x), len(y)), reverse=True)

# 'none of the above' added
types.append('none of the above')

# Function modified to return appropriate VQA question type
def determine_ques_type(question):
    # Return the answer and question types for the given question
    global types
    # print question
    for ts in types:
        question = ' '.join(question.split())
        tsq = ts.split(",")[0]
        if question.lower().startswith(tsq):
            return ts.split(",")[1]

    return 'none of the above'

with open("input_data/vqa_val_obj_tups_ans.json", "r") as f:
    qs = json.loads(f.read())
    new = []
    for ix, q in enumerate(qs):

        qtype = determine_ques_type(q["q"])
        if qtype == "yes/no":
            if q["ans"] == "no":
                continue
        elif qtype == "number":
            if (q["ans"] == "0") or (q["ans"] == "none"):
                continue

        new.append(q)

    with open("input_data/vqa_val_obj_tups_filtered.json", "w") as f:
        json.dump(new, f, indent=4)