
require 'torch'
require 'nn'
require 'image'

-- Preprocess an image before passing it to a Caffe model.
-- We need to rescale from [0, 1] to [0, 255], convert from RGB to BGR,
-- and subtract the mean pixel.
function utils.preprocess(img)
    local mean_pixel = torch.DoubleTensor({103.939, 116.779, 123.68})
    local perm = torch.LongTensor{3, 2, 1}
    img = img:index(1, perm):mul(256.0)
    mean_pixel = mean_pixel:view(3, 1, 1):expandAs(img)
    img:add(-1, mean_pixel)
    return img
end

require 'loadcaffe'

cmd = torch.CmdLine()
cmd:text('Options')

cmd:option('-split', 'val', 'train/val')
cmd:option('-batch_size', 64, 'batch size')
cmd:option('-debug', 0, 'set debug = 1 for lots of prints')
-- bookkeeping
cmd:option('-seed', 981723, 'Torch manual random number generator seed')
cmd:option('-proto_file', '/srv/share/home/virajp/code/toaim/models/VGG_ILSVRC_19_layers_deploy.prototxt')
cmd:option('-model_file', '/srv/share/home/virajp/code/toaim/models/VGG_ILSVRC_19_layers.caffemodel')
cmd:option('-data_dir', 'visgen', 'Data directory.')
cmd:option('-feat_layer', 'relu7', 'Layer to extract features from')
cmd:option('-input_image_dir', '/srv/share/data/mscoco/images/val2014/', 'Image directory')
cmd:option('-gpuid', -1, '0-indexed id of GPU to use. -1 = CPU')
-- cmd:option('-num_gpus', -1, 'Number of GPU\'s')

opt = cmd:parse(arg or {})
torch.manualSeed(opt.seed)

if opt.gpuid >= 0 then
    local ok, cunn = pcall(require, 'cunn')
    local ok2, cutorch = pcall(require, 'cutorch')
    if not ok then print('package cunn not found!') end
    if not ok2 then print('package cutorch not found!') end
    if ok and ok2 then
        print('using CUDA on GPU ' .. opt.gpuid .. '...')
        -- cutorch.setDevice(1)
        cutorch.manualSeed(opt.seed)
    else
        print('If cutorch and cunn are installed, your CUDA toolkit may be improperly configured.')
        print('Check your CUDA toolkit installation, rebuild cutorch and cunn, and try again.')
        print('Falling back to CPU mode')
        opt.gpuid = -1
    end
end


-- Get image id's
print("Populating image list ... ")
imlist = {}
fp = io.popen("ls " .. opt.input_image_dir)
im = fp:read()
while im ~= nil do
    table.insert(imlist, im)
    im = fp:read()
end

cnn = loadcaffe.load(opt.proto_file, opt.model_file)
if opt.gpuid >= 0 then
    cnn = cnn:cuda()
end

cnn_fc7 = nn.Sequential()

for i = 1, #cnn.modules do
    local layer = cnn:get(i)
    local name = layer.name
    cnn_fc7:add(layer)
    if name == opt.feat_layer then
        break
    end
end

cnn_fc7:evaluate()

if opt.gpuid >= 0 then
    cnn_fc7 = cnn_fc7:cuda()
<<<<<<< HEAD:false-premise-experiments/utils/extract_fc7.lua
    -- gpus = torch.range(0, opt.gpuid-1):totable()
    -- print(gpus)
    -- dpt = nn.DataParallelTable(1):add(cnn_fc7, gpus):cuda()
=======
        gpus = torch.range(1, opt.gpuid+1):totable()
    print(gpus)
    dpt = nn.DataParallelTable(1):add(cnn_fc7, gpus):cuda()
>>>>>>> 9086ba3e3e4b7505950ad3c0bfb549abdee5b6b6:false-premise-experiments/extract_fc7.lua
end

tmp_image_id = {}
for i = 1, #imlist do
    tmp_image_id[imlist[i]] = 1
end

image_id = {}
idx = 1
for i, v in pairs(tmp_image_id) do
    image_id[idx] = i
    idx = idx + 1
end

fc7 = torch.DoubleTensor(#image_id, 4096)
idx = 1

if opt.gpuid >= 0 then
    fc7 = fc7:cuda()
end

print("Beginning extraction ... ")
local skip_count = 0
repeat
    local timer = torch.Timer()
    img_batch = torch.zeros(opt.batch_size, 3, 224, 224)
    img_id_batch = {}
    for i = 1, opt.batch_size do
        if not image_id[idx] then
            break
        end
        local fp = path.join(opt.input_image_dir, image_id[idx])
        if opt.debug == 1 then
            print(idx)
            print(fp)
        end
        local ok, img = pcall(image.load, fp, 3)
        if ok then
            img_batch[i] = utils.preprocess(image.scale(img, 224, 224))
            img_id_batch[i] = image_id[idx]
        else
            print("Skipping ", fp)
            skip_count = skip_count + 1
        end
        idx = idx + 1
    end

    if opt.gpuid >= 0 then
        img_batch = img_batch:cuda()
    end

    fc7_batch = cnn_fc7:forward(img_batch:narrow(1, 1, #img_id_batch))

    for i = 1, fc7_batch:size(1) do
        if opt.debug == 1 then
            print(idx - fc7_batch:size(1) + i - 1)
        end
        fc7[idx - fc7_batch:size(1) + i - 1]:copy(fc7_batch[i])
    end

    if opt.gpuid >= 0 then
        cutorch.synchronize()
    end

    local time = timer:time().real
    print(idx-1 .. '/' .. #image_id .. " " .. time)
    collectgarbage()
until idx >= #image_id

print("Skip count:", skip_count)
torch.save(path.join(opt.data_dir, opt.split .. '_fc7.t7'), fc7)
torch.save(path.join(opt.data_dir, opt.split .. '_fc7_image_id.t7'), image_id)
