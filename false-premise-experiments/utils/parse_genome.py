import sys
import cPickle

sys.path.insert(0, 'visual_genome_python_driver')

import src.local as vg

def parse():
	dataDir = "./VisualGenome/"
	imageDataDir = "./data/by-id/"

	# vg.SaveSceneGraphsById(dataDir=dataDir, imageDataDir=imageDataDir)
	print("Parsing Scene Graphs .. this might take a while.")
	scene_graphs = vg.GetSceneGraphs(startIndex=0, endIndex=1000, minRels=1, \
								 dataDir=dataDir, imageDataDir=imageDataDir)
	return scene_graphs

def build_rel2image(scene_graphs, store_path):
	rel2image = {}
	for ix, scene_graph in enumerate(scene_graphs):
		if ix%100 == 0:
			print ix
		for relationship in scene_graph.relationships:
			try:
				key = str(relationship.subject).lower() + "_" + str(relationship.object).lower()
				obj = {"imid": scene_graph.image.id,"rel": str(relationship.subject).lower() + "_" + str(relationship.predicate).lower() + "_" + str(relationship.object).lower() }
				if key in rel2image:
					rel2image[key].append(obj)
				else:
					rel2image[key] = [obj]
			except Exception as e:
				print "Skipping one"

	cPickle.dump( rel2image, open(store_path + "/rel2image.p", "wb") )
	# rel2image = cPickle.load(store_path + "/rel2image.p", "rb")