import os
import numpy as np
import torchfile
import cPickle

from annoy import AnnoyIndex

N_TREES = 10
featlen = 100

def load_fc7(fc7_path):

	print("Loading fc7 ...")

	visgenfeats_pca = np.load(fc7_path + "/visgenfeats_pca.npy")
	visgen_image_id = torchfile.load(fc7_path + "/visgen_image_id.t7")

	vqatrainfeats_pca = np.load(fc7_path + "/vqatrainfeats_pca.npy")
	vqatrainlist = torchfile.load(fc7_path + "/train_fc7_image_id.t7")

	return visgenfeats_pca, visgen_image_id, vqatrainfeats_pca, vqatrainlist

def build_fc7_index(visgenfeats_pca, visgen_image_id, vqatrainlist, path):

	print("Building fc7 index ...")

	ix2imid, vqatrainimid2ix, visgenimid2ix = {}, {}, {}
	index = AnnoyIndex(featlen, metric="euclidean")
	for ix, imid in enumerate(visgen_image_id):
		ix2imid[ix] = imid
		visgenimid2ix[imid] = ix
		# index.add_item(ix, visgenfeats_pca[ix])

	for ix, imid in enumerate(vqatrainlist):
		vqatrainimid2ix[imid] = ix

	# index.build(N_TREES)
	# index.save(path + "/index_visgen.ann")

	cPickle.dump(ix2imid, open(path + "/ix2imid.p", "wb"))
	cPickle.dump(vqatrainimid2ix, open(path + "/vqatrainimid2ix.p", "wb"))
	cPickle.dump(visgenimid2ix, open(path + "/visgenimid2ix.p", "wb"))

	# index.load(path + "/index_visgen.ann")
	# ix2imid = cPickle.load(open(path + "/ix2imid.p", "rb"))
	# vqatrainimid2ix = cPickle.load(open(path + "/vqatrainimid2ix.p", "rb"))
	# visgenimid2ix = cPickle.load(open(path + "/visgenimid2ix.p", "rb"))

	return index, ix2imid, vqatrainimid2ix, visgenimid2ix

def main():
	cwd = os.getcwd()
	store_path = cwd + "/store"
	visgenfeats_pca, visgen_image_id, vqatrainfeats_pca, vqatrainlist = load_fc7(store_path)
	visgen_index, ix2imid, vqatrainimid2ix, visgenimid2ix = build_fc7_index(visgenfeats_pca, visgen_image_id, vqatrainlist, store_path)

if __name__ == "__main__":					
	main()