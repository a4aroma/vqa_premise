import json
import operator
import nltk
import utils.nltk_helpers as nltkh
'''
	1. Find interactions, threshold frequency, reduce to single word
	2. Separate by PoS
	3. Find antonyms for each using libraries / manually
'''

def reduce_multiword(multiword):
	pass

def pos_tag(interaction):
	interaction_pos = nltk.pos_tag(interaction.split(" "))
	if len(interaction_pos) == 1:
		pos = interaction_pos[0][1]
	else:
		pos = interaction_pos[0][1]
		for word_pos in interaction_pos:
			if nltkh.is_verb(word_pos[1]):
				pos = word_pos[1]
	return pos

def build_rel_antonyms():

	THRESHOLD = 10
	
	with open("input_data/vqa_rel_tups_filtered.json") as f:
		rels = json.load(f)
		int2count = {}
		for ix, rel in enumerate(rels):
			interaction = rel["tup"][1]
			if interaction in int2count:
				int2count[interaction] += 1
			else:
				int2count[interaction] = 1

		sorted_ints = sorted(int2count.items(), key=operator.itemgetter(1), reverse=True)
		sorted_ints_filtered = [sorted_int for sorted_int in sorted_ints if sorted_int[1] >= THRESHOLD]

		sorted_ints_filtered_pos = [pos_tag(sorted_int[0]) for sorted_int in sorted_ints_filtered]

		ints = zip(sorted_ints_filtered, sorted_ints_filtered_pos)

		verb_ints = [interaction[0] for interaction in ints if nltkh.is_verb(interaction[1])]
		nonverb_ints = [interaction[0] for interaction in ints if interaction[0] not in verb_ints]

	# with open("input_data/verb_ints.json", "w") as f1, open("input_data/nonverb_ints.json", "w") as f2:
	# 	json.dump(verb_ints, f1, indent=4)
	# 	json.dump(nonverb_ints, f2, indent=4)
	nonverb_int_names = [nonverb_int[0] for nonverb_int in nonverb_ints]
	nonverb_rels = []
	for ix, rel in enumerate(rels):
		interaction = rel["tup"][1]
		if interaction in nonverb_int_names:
			nonverb_rels.append(rel)
	with open("input_data/vqa_rel_tups_nonverb.json", "w") as f:
		json.dump(nonverb_rels, f, indent=4)

def main():
	build_rel_antonyms()

if __name__ == "__main__":					
	main()
