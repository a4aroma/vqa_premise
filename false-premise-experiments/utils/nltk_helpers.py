import nltk
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer
import re

wnl = WordNetLemmatizer()

def pos_lemmatize(target, target_pos, source_list):
    target_pos_wn = penn_to_wn(target_pos)
    target_lemma = wnl.lemmatize(target, target_pos_wn)

    pred_pos_list = [nltk.pos_tag([pred])[0][1] for pred in source_list]
    pred_pos_wn_list = [penn_to_wn(pred_pos) for pred_pos in pred_pos_list]

    pred_lemma_list = [wnl.lemmatize(tup[0], tup[1]) for tup in zip(source_list, pred_pos_wn_list)]

    return target_lemma, pred_lemma_list

def interaction_is_verb(pred, question):
    q = question.replace("?", "").split(" ")
    q_pos = nltk.pos_tag(q)
    pred_pos = [q_pos[i] for i in range(len(q_pos)) if q[i] == pred]
    return is_verb(pred_pos[0][1]), pred_pos

def is_noun(tag):
    return tag in ['NN', 'NNS', 'NNP', 'NNPS']

def is_verb(tag):
    return tag in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']


def is_adverb(tag):
    return tag in ['RB', 'RBR', 'RBS']


def is_adjective(tag):
    return tag in ['JJ', 'JJR', 'JJS']


def penn_to_wn(tag):
    if is_adjective(tag):
        return wn.ADJ
    elif is_noun(tag):
        return wn.NOUN
    elif is_adverb(tag):
        return wn.ADV
    elif is_verb(tag):
        return wn.VERB
    return wn.NOUN # Default to noun


def is_sister_term(pred, target_pred):  
    pred_pos = nltk.pos_tag([pred])[0][1]
    target_pos = nltk.pos_tag([target_pred])[0][1]
    target_lemma_pred = wnl.lemmatize(target_pred, penn_to_wn(target_pos))
    lemma_pred = wnl.lemmatize(pred, penn_to_wn(pred_pos))
    wn_sister_terms = wn.synsets(target_lemma_pred)[0].hypernyms()[0].hyponyms() # TODO: Add word-sense disambiguation
    pred_synsets = wn.synsets(lemma_pred)
    if len(set(pred_synsets) & set(wn_sister_terms)) == 0: 
        return False
    return True

def main():
    target_lemma, pred_lemma_list = pos_lemmatize("hello", ["this", "is", "a", "test", "sentence"])
    print(target_lemma, pred_lemma_list)
if __name__ == "__main__":                  
    main()
