import json
import operator
with open("relationships.json", "r") as f:
	data = json.loads(f.read())

	cococategories = "man, woman, window, person, bicycle, car, motorcycle, airplane, bus, train, truck, boat, traffic light, fire hydrant, stop sign, parking meter, bench, bird, cat, dog, horse, sheep, cow, elephant, bear, zebra, giraffe, backpack, umbrella, handbag, tie, suitcase, frisbee, skis, snowboard, sports ball, kite, baseball bat, baseball glove, skateboard, surfboard, tennis racket, bottle, wine, glass, cup, fork, knife, spoon, bowl, banana, apple, sandwich, orange, broccoli, carrot, hot dog, pizza, donut, cake, chair, couch, potted plant, bed, dining table, toilet, tv, laptop, mouse, remote, keyboard, cell phone, microwave, oven, toaster, sink, refrigerator, book, clock, vase, scissors, teddy bear, hair, drier, toothbrush".split(",")
	cococategories = [category.strip() for category in cococategories]

	rel2count = {}
	for ix, imobj in enumerate(data):
		imid = imobj["image_id"]
		for ix, rel in enumerate(imobj["relationships"]):
			subject = rel["subject"]["name"]
			predicate = rel["predicate"]
			obj = rel["object"]["name"]

			if subject not in cococategories:
				continue
			relationship = subject + "_" + predicate + "_" + obj
			if relationship in rel2count:
				rel2count[relationship] += 1
			else:
				rel2count[relationship] = 1

	sorted_rels = sorted(rel2count.items(), key=operator.itemgetter(1), reverse=True)
	with open("rels.json", "w") as f:
		json.dump(sorted_rels, f, indent=4)