#!/bin/bash
IP_DIR=../../attribute_tuples_v2
OP_DIR=./attr_dataset_ant
start_=1
stop_=100
step=1
for ((i=$start_;i<=$stop_;i=i+$step));do
        tests="test"$((i-1))"s"
	screen -dmS $tests sh -c "python find_negative_attribute.py  $IP_DIR/op_$i.json $OP_DIR/op_$i.json 0;exec /bin/bash"
done
