import os
import json
import sys
import cPickle
import string
import pprint
import collections
from tqdm import *

from annoy import AnnoyIndex
import numpy as np

import utils.w2v as w2v_helpers
import utils.load_fc7 as fc7_helpers
import utils.nltk_helpers as nltkh
import soft_retrieval as retrieve

NUM_NEIGHBORS = 20
cache = {}
sim_thresh = 0.3

def filter_word(word):
	valid_chars = string.ascii_letters + string.digits + ' 	'
	word = ''.join(c for c in word if c in valid_chars)
	return word

def soft_retrieval(obj1, obj2, target_pred, w2v):
	key = " ".join([obj1, obj2])
	score = retrieve.compute_score(key, w2v)
	nns = rel_index.get_nns_by_vector(score, NUM_NEIGHBORS)
	nn_rels = [rel_list[i] for i in nns]

	# print("Neighbors:", nn_rels)

	nn_preds = [rel.split("_")[1] for rel in nn_rels]

	# Pick farthest interaction from pool

	target_lemma, pred_lemma_list = nltkh.pos_lemmatize(target_pred, nn_preds)
	w2v_sims = [retrieve.similarity_score(target_lemma, pred_lemma, w2v) for pred_lemma in pred_lemma_list]

	w2v_zips = zip(nn_rels, w2v_sims)

	selected_rel = max(w2v_zips, key = lambda t: t[1])[0]

	selected_rel = "_".join([filter_word(word) for word in selected_rel.split("_")])

	# print("Negative:", selected_rel)
	
	# Look up candidate images 
	candidate_imids = rel2imids[selected_rel]

	return candidate_imids, selected_rel

def hard_retrieval(obj1, obj2, target_pred, target_pred_pos, rel2image, w2v):
	global cache

	key = "_".join([obj1, obj2])

	if key not in rel2image:
		return

	imglist = rel2image[key]

	full_rel = "_".join([obj1, target_pred, obj2])

	if full_rel in cache:
		selected_rel = cache[full_rel]
	else:
		# Pick a relation based on some logic
		rel_imgs = [img["imid"] for img in imglist]
		rel_preds = [img["rel"].split("_")[1] for img in imglist]

		target_lemma, pred_lemma_list = nltkh.pos_lemmatize(target_pred, target_pred_pos, rel_preds)

		w2v_dists = [retrieve.similarity_score(target_lemma, pred_lemma, w2v) for pred_lemma in pred_lemma_list]

		w2v_zips = zip(rel_imgs, rel_preds, w2v_dists)

		w2v_zips.sort(key = lambda t: t[2], reverse=True)

		candidate_imids = [rel[0] for rel in w2v_zips] # Least to most similar
		candidate_rels = [obj1 + "_" + rel[1] + "_" + obj2 for rel in w2v_zips]

		candidate_imlist = w2v_zips[:100]

		selected2imid = collections.OrderedDict()
		for tup in candidate_imlist:
			full_rel = "_".join([obj1, tup[1], obj2])
			if tup in selected2imid:
				selected2imid[full_rel].append(tup[0])
			else:
				selected2imid[full_rel] = [tup[0]]
		
		# HITL Pipeline
		# for ix, rel in enumerate(selected2imid.keys()):
		# 	print(ix, ": ", rel)

		# selected_rel = raw_input('--> ')

		selected_rel = 0

		if selected_rel == "s":
			return True, [], [] 
		else:
			selected_rel =  selected2imid.keys()[int(selected_rel)]
			cache[full_rel] = selected_rel

	candidate_imids = [rel["imid"] for rel in rel2image[key] if rel["rel"] == selected_rel]
	candidate_rels = [selected_rel for i in range(len(candidate_imids))]

	return False, candidate_imids, candidate_rels

def hard_retrieval_antonyms(obj1, obj2, target_pred, target_pred_pos, rel2image, nv_antonyms, subjpred2image, w2v):

	antonym_list = nv_antonyms[target_pred]

	candidate_imids, candidate_rels = [], []
	if antonym_list[0] == False:
		"""Negate object for these"""
		
		key = "_".join([obj1, target_pred])
		# print("Subjpredkey", key)
		if key in subjpred2image:
			imglist = subjpred2image[key]
			for img in imglist:
				tup = img["rel"].split("_")
				if tup[-1] != obj2:
					altobj = tup[-1]
					break

			full_rel = "_".join([obj1, target_pred, altobj])
			# print(full_rel)
			imlist = [img["imid"] for img in imglist if img["rel"] == full_rel]
			candidate_imids.extend(imlist)
			candidate_rels.extend([full_rel for i in range(len(imlist))])
	else:
		"""Negate interaction for these"""
		key = "_".join([obj1, obj2])
		imglist = rel2image[key]
		# print("Antonym List:", antonym_list)
		for antonym in antonym_list:

			full_rel = "_".join([obj1, antonym, obj2])
			
			# print(full_rel)
			imlist = [img["imid"] for img in imglist if img["rel"] == full_rel]
			if len(imlist) > 0:
				candidate_imids.extend(imlist)
				candidate_rels.extend([full_rel for i in range(len(imlist))])

	return (len(candidate_imids)==0), candidate_imids, candidate_rels

def main():
	global cache

	cwd = os.getcwd()
	input_data_path = cwd + "/input_data"
	input_json_path = input_data_path + "/vqa_rel_tups_filtered.json"
	store_path = cwd + "/store"
	output_data_path = cwd + "/output_data"
	illegal_preds = ["having", "has"]
	illegal_objects = ["objects", "light", "sky", "plate"]
	nv_antonyms = cPickle.load(open(store_path + "/nv_antonyms.p", "rb"))
	with open(input_json_path, "r") as f, open(output_data_path + "/cache.json", "r") as f2:
		data = json.loads(f.read())
		# cache = json.loads(f2.read())

		# w2v = w2v_helpers.load_model()
		w2v = {}

		rel_index = AnnoyIndex(300, metric="euclidean")
		rel_index.load(store_path + "/rel_index.ann")
		rel_list = cPickle.load(open(store_path + "/rel_index.p", "rb"))

		visgenfeats_pca, visgen_image_id, vqatrainfeats_pca, vqatrainlist = fc7_helpers.load_fc7(store_path)
		visgen_index, ix2imid, vqatrainimid2ix, visgenimid2ix = fc7_helpers.build_fc7_index(visgenfeats_pca, visgen_image_id, vqatrainlist, store_path)

		rel2imids = cPickle.load(open(store_path +  "/rel2imids.p"))
		rel2image = cPickle.load(open(store_path + "/rel2image.p", "rb"))

		subjpred2image = cPickle.load(open(store_path + "/subjpred2image.p", "rb"))
		output = []

		for ix in tqdm(range(len(data))):
			qobj = data[ix]

			try:
				obj1 = filter_word(qobj["tup"][0])
				obj2 = filter_word(qobj["tup"][2])

				target_pred = str(filter_word(qobj["tup"][1]))

				if obj1 in illegal_objects:
					continue

				if target_pred not in nv_antonyms:
					continue

				is_verb, target_pred_pos = nltkh.interaction_is_verb(target_pred, qobj["q"])
				if (target_pred in illegal_preds) or (is_verb):
					continue
				
				# print("Source:", qobj["tup"])

				# Retrieve NN subj+obj relations

				# 1. Soft retrieval
				# candidate_imids, selected_rel = soft_retrieval(obj1, obj2, target_pred, w2v)

				# 2. Hard retrieval
				skip, candidate_imids, candidate_rels = hard_retrieval_antonyms(obj1, obj2, target_pred, target_pred_pos, rel2image, nv_antonyms, subjpred2image, w2v)

				if skip:
					continue

				# Pick candidate image farthest from source

				if len(candidate_imids) > 1:
					impath = "COCO_train2014_" + str(qobj["imid"]).zfill(12) + ".jpg"
					source_fc7 = vqatrainfeats_pca[vqatrainimid2ix[impath]]

					fc7_distances = [np.linalg.norm(source_fc7 - visgenfeats_pca[visgenimid2ix[str(imid) + ".jpg"]]) for imid  in candidate_imids]

					fc7_zips = zip(candidate_imids, candidate_rels, fc7_distances)
					
					selected_neg_img = min(fc7_zips, key = lambda t: t[2])
				else:
					selected_neg_img = candidate_imids

				selected_rel = candidate_rels[0]
				
				output.append({
					"q": qobj["q"],
					"rel_imid": qobj["imid"],
					"irr_imid": selected_neg_img[0],
					"rel_tuple": "_".join(qobj["tup"]),
					"irr_tuple": selected_rel
				})

				# if len(output)%10 == 0:
				# 	print("Completed ", len(output))
					
			except Exception, e:
				print("Error:", e)
			
		with open(output_data_path + "/output.json", "w") as f:
			json.dump(output, f, indent=4)
		with open(output_data_path + "/cache.json", "w") as f:
			json.dump(cache, f, indent=4)

if __name__ == "__main__":					
	main()