import json
import matplotlib.pyplot as plt
from progressbar import Bar, ETA, Percentage, ProgressBar    
import numpy as np
"""
	Plot (avg. correct premise answer confidence
							v/s
		  	  avg. source answer confidence)
	Variants:
	a) All
	b) Only nonbinary premises
	c) a + nonbinary sources
"""

def plot(x, y, plottype, xlab, ylab):
	""" Plotting"""
	figure, ax = plt.subplots()

	print len(x)
	print len(y)
	
	minx = int(min(x))
	miny = int(min(y))
	maxx = int(max(x))
	maxy = int(max(y))

	# print minx, " ", maxx, " ", miny, " ", maxy

	gridx = np.linspace(minx, maxx, 20)
	gridy = np.linspace(miny, maxy, 20)
	
	grid, _, _ = np.histogram2d(x, y, bins=[gridx, gridy])

	plt.figure()
	plt.title("Premise VQA correlation (N = %d)" % len(x))
	plt.xlabel(xlab, fontsize = 15)
	plt.ylabel(ylab, fontsize = 15)	
	
	ax.plot(x, y, 'ro')
	plt.pcolormesh(gridx, gridy, grid)
	plt.colorbar()
	plt.savefig("confidence_correlation_" + plottype + ".png")

	plt.draw()
	plt.pause(1)
	raw_input("<Hit Enter To Close>")
	plt.close()

def main():
	offset = 10000000
	with open("Results/OpenEnded_mscoco_co-atten_results.json", "r") as f1, \
		 open("Annotations/OpenEnded_mscoco_val2014_annotations_augmented.json", "r") as f2, \
		 open("Questions/Questions_Val_mscoco/OpenEnded_mscoco_val2014_questions_augmented.json", "r") as f3:

		print "Loading results and annotations..."
		predictions = json.loads(f1.read())
		annotations = json.loads(f2.read())["annotations"]
		questions = json.loads(f3.read())["questions"]
		print  "Loaded successfully.."

		total = len(predictions)
		# Create mapping from question id's to index
		qtoi = {prediction["question_id"]:i for i, prediction in enumerate(predictions)}

		results = []
		widgets = ['Evaluating ', Percentage(), ' ', Bar(marker='#',left='[',right=']'),
			   		' ', ETA()]
		pbar = ProgressBar(widgets=widgets)

		X, Y, X1, Y1, X2, Y2, X3, Y3, X4, Y4 = [], [], [], [], [], [], [], [], [], []
		points_lost = 0
		for i in pbar(range(0, total)):
			if (predictions[i]["question_id"]>offset):
				continue

			# Evaluate if source question answered correctly
			if predictions[i]["answer"] == annotations[i]["multiple_choice_answer"]:
				source_correct = True
			else:
				source_correct = False
			
			premise_question_id = offset + (predictions[i]["question_id"] * 20) + 1

			premise_correct, premise_total, premise_valid_total, nonbinary_premise_valid_total, binary_premise_valid_total = 0, 0, 0, 0, 0
			# binary_correct = 0
			# binary_total = 0
			premise_correct_confidence, premise_confidence, nonbinary_confidence, binary_confidence = 0, 0, 0, 0
			if premise_question_id in qtoi:
				id = qtoi[premise_question_id]
				nonbinary = True
				while id<len(predictions)-1:
					premise_confidence += predictions[id]["answer_prob"]
					premise_total += 1

					if predictions[id]["gt_prob"] != 0:
						premise_correct_confidence += predictions[id]["gt_prob"]
						premise_valid_total += 1
					else:
						points_lost += 1
						# if annotations[id]["multiple_choice_answer"] != "yes" and annotations[id]["multiple_choice_answer"] != "no":
					 # 		nonbinary_confidence += predictions[id]["answer_prob"]
					 # 		nonbinary_premise_valid_total += 1

					 # 	if annotations[id]["multiple_choice_answer"] == "yes" or annotations[id]["multiple_choice_answer"] == "no":
					 # 		binary_confidence += predictions[id]["gt_prob"]
					 # 		binary_premise_valid_total += 1


					
					# if predictions[id]["answer"] == annotations[id]["multiple_choice_answer"]:
					# 	premise_correct += 1
						
						# if predictions[id]["answer"] == "yes" or predictions[id]["answer"] == "no":
						# 	binary_correct += 1

					if predictions[id+1]["question_id"] == (premise_question_id+1):
						id += 1
						premise_question_id += 1
					else:
						break

			if premise_total == 0:
				# perc = 0
				avg_correct_confidence, avg_confidence, avg_nonbin_confidence, avg_bin_confidence = 0, 0, 0, 0
				# binary_perc = 0
				
			else:
				# perc = round(premise_correct / float(premise_total), 3)
				avg_confidence = round(premise_confidence / float(premise_total), 3)	
				# binary_perc = round(binary_correct / float(binary_total), 3)

				if premise_valid_total == 0:
					avg_correct_confidence = 0
				else:
					avg_correct_confidence = round(premise_correct_confidence / float(premise_valid_total), 3)	

				# if nonbinary_premise_valid_total == 0:
				# 	avg_nonbin_confidence = 0
				# else:
				# 	avg_nonbin_confidence = round(nonbinary_confidence / float(nonbinary_premise_valid_total), 3)

				# if binary_premise_valid_total == 0:
				# 	avg_bin_confidence = 0
				# else:
				# 	avg_bin_confidence = round(binary_confidence / float(binary_premise_valid_total), 3)

			if avg_correct_confidence > 0 and premise_total < 7:
				X.append(avg_correct_confidence)
				Y.append(predictions[i]["answer_prob"])

			if avg_correct_confidence > 0 and predictions[i]["gt_prob"] > 0 and premise_total < 7:
				X4.append(avg_correct_confidence)
				Y4.append(predictions[i]["gt_prob"])

			# if avg_nonbin_confidence > 0:
			# 	X2.append(avg_nonbin_confidence)
			# 	Y2.append(predictions[i]["answer_prob"])					

			if avg_confidence > 0 and premise_total < 7:
				X1.append(avg_confidence)
				Y1.append(predictions[i]["answer_prob"])

			# if avg_bin_confidence > 0:
			# 	X3.append(avg_bin_confidence)
			# 	Y3.append(predictions[i]["answer_prob"])

			# Log and dump results
			results.append({
				"question_id": predictions[i]["question_id"],
				"source": source_correct,
				"premise_correct": premise_correct,
				"premise_total": premise_total,
				"avg_correct_confidence": avg_correct_confidence,
				"avg_confidence": avg_confidence
				# "perc": perc,
				# "binary_correct": binary_correct,
				# "binary_total": binary_total,
				# "binary_perc": binary_perc
				})

		with open("results.json", "w") as f:
			json.dump(results, f)

		print(str(points_lost) + " points lost out of " + str(total))
		# Plot
		print("Plotting v/s GT")
		plot(X, Y, "gtvpred", "Average confidence for premise ground truth answers", "Confidence for source predicted answer")

		print("Plotting v/s GT")
		plot(X4, Y4, "gtvgt", "Average confidence for premise ground truth answers", "Confidence for source ground truth answer")

		print("Plotting v/s Predictions")
		plot(X1, Y1, "pred", "Average confidence for premise predicted answers", "Confidence for source predicted answer")

		# print("Plotting v/s Nonbin predictions")
		# plot(X2, Y2, "nonbin", "Average confidence for premise ground truth answer", "Average confidence for source predicted answer")

		# print("Plotting v/s GT predictions")
		# plot(X3, Y3, "bin", "Average confidence for premise ground truth answer", "Average confidence for source predicted answer")

if __name__ == "__main__":
	main()
	