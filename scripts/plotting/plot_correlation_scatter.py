import os
import sys
import json
import matplotlib.pyplot as plt
import collections
from progressbar import Bar, ETA, Percentage, ProgressBar    
import numpy as np

"""
	Plot (avg. correct premise answer confidence
							v/s
		  	  avg. source answer confidence)
	Variants:
	a) All
	b) Only nonbinary premises
	c) a + nonbinary sources
"""

def plot(X, Y):
	
	figure, ax = plt.subplots()

	ax.set_title("Premise VQA correlation")
	ax.set_xlabel("Average confidence for premise ground truth", fontsize = 9)
	ax.set_ylabel("Confidence for source ground truth", fontsize = 9)	
		
	plt.scatter(X, Y)
	plt.savefig("confidence_correlation.png")
	plt.show()
	plt.close()

def get_dict(results, C):
	print C
	Y_C = {}
	Y_T = {}
	if C < 10:
		return {}
	for result in results:
		if result["premise_total"] == 0:
			X = 0
		else:
			X = round(result["premise_correct"] / float(result["premise_total"]), 3)
		if result["source"]:
			if X not in Y_C:
				Y_C[X] = 1
			else:
				Y_C[X] += 1
		if X not in Y_T:
			Y_T[X] = 1
		else:
			Y_T[X] += 1

	D = {}
	counts = {}
	for k, v in Y_C.iteritems():
		# if Y_T[k] < 1000:
		# 	continue
		D[k] = Y_C[k] / float(Y_T[k])
		counts[k] = Y_T[k]
	return {'D':D, 'counts':counts}

if __name__ == "__main__":
	offset = 10000000
	print "Loading results and annotations..."
	with open("Results/OpenEnded_mscoco_co-atten_results.json", "r") as f1, \
		 open("Annotations/OpenEnded_mscoco_val2014_annotations_augmented.json", "r") as f2, \
		 open("Questions/Questions_Val_mscoco/OpenEnded_mscoco_val2014_questions_augmented.json", "r") as f3:
		
		print  "Loaded successfully.."

		predictions = json.loads(f1.read())
		annotations = json.loads(f2.read())["annotations"]
		questions = json.loads(f3.read())["questions"]

		count = 0
		total = len(predictions)

		qtoi = {prediction["question_id"]:i for i, prediction in enumerate(predictions)}

		results = []
		widgets = ['Evaluating ', Percentage(), ' ', Bar(marker='#',left='[',right=']'),
			   		' ', ETA()]
		pbar = ProgressBar(widgets=widgets)
		X, Y = [], []

		for i in pbar(range(0, total)):
			if (predictions[i]["question_id"]>offset):
				continue

			# Evaluate if source question answered correctly
			if predictions[i]["answer"] == annotations[i]["multiple_choice_answer"]:
				count += 1
				source_correct = True
			else:
				source_correct = False
			
			premise_question_id = offset+ (predictions[i]["question_id"] * 20) + 1
			premise_correct = 0
			binary_correct = 0
			premise_total = 0
			binary_total = 0
			premise_confidence = 0
			if premise_question_id in qtoi:
				id = qtoi[premise_question_id]
				# print id
				while id<len(predictions)-1:
					premise_total += 1

					premise_confidence += predictions[i]["gt_prob"]

					if annotations[id]["multiple_choice_answer"] == "yes" or annotations[id]["multiple_choice_answer"] == "no":
						binary_total += 1

					if predictions[id]["answer"] == annotations[id]["multiple_choice_answer"]:
						premise_correct += 1
						if predictions[id]["answer"] == "yes" or predictions[id]["answer"] == "no":
							binary_correct += 1

					if predictions[id+1]["question_id"] == (premise_question_id+1):
						id += 1
						premise_question_id += 1
					else:
						break

			if premise_total == 0:
				perc = 0
				binary_perc = 0
				avg_confidence = 0
			else:
				perc = round(premise_correct / float(premise_total), 3)
				binary_perc = round(binary_correct / float(binary_total), 3)
				avg_confidence = round(premise_confidence / float(premise_total), 3)

			if avg_confidence > 0:
				X.append(avg_confidence)
				Y.append(predictions[i]["answer_prob"])

			results.append({
				"question_id": predictions[i]["question_id"],
				"source": source_correct,
				"premise_correct": premise_correct,
				"premise_total": premise_total,
				"perc": perc,
				"binary_correct": binary_correct,
				"binary_total": binary_total,
				"binary_perc": binary_perc,
				"avg_confidence": avg_confidence
				})

		with open("results.json", "w") as f:
			json.dump(results, f)

		# D = get_dict(results, len(results))
		plot(X, Y)