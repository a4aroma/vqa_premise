import os
import sys
import json
import matplotlib.pyplot as plt
import collections
from progressbar import Bar, ETA, Percentage, ProgressBar    
import numpy as np
"""
For every question, note:
- correctness of source answer
- correctness of premise answers
Caveat:
- Remove duplicates from premise questions
"""

def plot(D, premise_count, count, counts):
	D = collections.OrderedDict(sorted(D.items()))
	
	figure, ax = plt.subplots()
	width = 1.5
	# freq_series = pd.Series.from_array(D.values())
	ind = np.arange(len(D.keys()))
	ax.bar(ind, D.values(), width, color="blue")
	ax.set_title("Premise VQA correlation (k=%s, N=%d)" % (str(premise_count), count))
	ax.set_xticks(ind+width/2)
	ax.set_xlabel('% premise questions answered correctly', fontsize = 9)
	ax.set_ylabel('% source questions answered correctly', fontsize = 9)	
	ax.set_xticklabels(D.keys())
	
	sorted_keys = sorted(counts.keys())
	labels = [counts[k] for k in sorted_keys]
	print labels
	
	for i, v in enumerate(labels):
		ax.text(i+.25, 0.1, str(v), color='black', fontweight='bold')

	plt.savefig("correlation_" + str(premise_count) + ".png")
	plt.show()
	plt.close()

def get_dict(results, C):
	print C
	Y_C = {}
	Y_T = {}
	if C < 10:
		return {}
	for result in results:
		if result["premise_total"] == 0:
			X = 0
		else:
			X = round(result["premise_correct"] / float(result["premise_total"]), 3)
		if result["source"]:
			if X not in Y_C:
				Y_C[X] = 1
			else:
				Y_C[X] += 1
		if X not in Y_T:
			Y_T[X] = 1
		else:
			Y_T[X] += 1

	D = {}
	counts = {}
	for k, v in Y_C.iteritems():
		# if Y_T[k] < 1000:
		# 	continue
		D[k] = Y_C[k] / float(Y_T[k])
		counts[k] = Y_T[k]
	return {'D':D, 'counts':counts}

offset = 10000000
print "Loading results and annotations..."
with open("Results/OpenEnded_mscoco_co-atten_results.json", "r") as f1, \
	 open("Annotations/OpenEnded_mscoco_val2014_annotations_augmented.json", "r") as f2:
	
	print  "Loaded successfully.."

	predictions = json.loads(f1.read())
	data = json.loads(f2.read())
	ground_truth = data["annotations"]
	count = 0
	total = len(predictions)

	qtoi = {prediction["question_id"]:i for i, prediction in enumerate(predictions)}

	results = []
	count = 0
	widgets = ['Evaluating ', Percentage(), ' ', Bar(marker='#',left='[',right=']'),
		   ' ', ETA()]
	pbar = ProgressBar(widgets=widgets)
	for i in pbar(range(0, total)): # HARDCODED - FIX THIS
		# Workaround - since everything is sorted
		if (predictions[i]["question_id"]>offset):
			continue

		# Evaluate if source question answered correctly
		if predictions[i]["answer"] == ground_truth[i]["multiple_choice_answer"]:
			count += 1
			source_correct = True
		else:
			source_correct = False
		
		# print "Accuracy is %f", (count/float(total))
		# Evaluate if premise questions answered correctly
		
		premise_question_id = offset+ (predictions[i]["question_id"] * 20) + 1
		premise_correct = 0
		binary_correct = 0
		premise_total = 0
		binary_total = 0
		if premise_question_id in qtoi:
			id = qtoi[premise_question_id]
			# print id
			while id<len(predictions)-1:
				premise_total += 1

				if ground_truth[id]["multiple_choice_answer"] == "yes" or ground_truth[id]["multiple_choice_answer"] == "no":
					binary_total += 1

				if predictions[id]["answer"] == ground_truth[id]["multiple_choice_answer"]:
					premise_correct += 1
					if predictions[id]["answer"] == "yes" or predictions[id]["answer"] == "no":
						binary_correct += 1

				if predictions[id+1]["question_id"] == (premise_question_id+1):
					id += 1
					premise_question_id += 1
				else:
					break

		if premise_total == 0:
			perc = 0
			binary_perc = 0
		else:
			perc = round(premise_correct / float(premise_total), 3)
			binary_perc = round(binary_correct / float(binary_total), 3)

		# if premise_total > 10:
		# 	print predictions[i]["question_id"]
		# if premise_total > 1:
		results.append({
			"question_id": predictions[i]["question_id"],
			"source": source_correct,
			"premise_correct": premise_correct,
			"premise_total": premise_total,
			"perc": perc,
			"binary_correct": binary_correct,
			"binary_total": binary_total,
			"binary_perc": binary_perc
			})

	with open("results.txt", "w") as f:
		json.dump(results, f)

	buckets = {}
	count = {}

	D = get_dict(results, len(results))
	plot(D["D"], "ALL", len(results), D["counts"])

	for result in results:
		if result["premise_total"] not in buckets:
			buckets[result["premise_total"]] = [result]
			count[result["premise_total"]] = 1
		else:
			buckets[result["premise_total"]].append(result)
			count[result["premise_total"]] += 1
	
	print buckets
	for k, v in buckets.iteritems():
		if k < 2 or k > 9:
			continue
		data  = get_dict(buckets[k], count[k])
		D = data["D"]
		counts = data["counts"]
		with open("results_" + str(k) + ".txt", "w") as f:
			json.dump(D, f)
		if D != {}:
			plot(D, k, count[k], counts)
	print Y
	

	
		

