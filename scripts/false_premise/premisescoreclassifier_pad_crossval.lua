-----------------------------------------------------------------------------
-- Todo: Use padding + clipping for premise softmax
-- Crossvalidate over clipping threshold
-----------------------------------------------------------------------------

require "nn"
repl=require "trepl"
local cjson = require "cjson"
local json = require "json"
local utils = {}

local offset = 10000000;
-----------------------------------------------------------------------------
-- Utility function --
-----------------------------------------------------------------------------

function utils.read_json(path)
  local file = io.open(path, "r")
  local text = file:read()
  file:close()
  local info = cjson.decode(text)
  return info
end

-----------------------------------------------------------------------------
--- Load Data ---
-----------------------------------------------------------------------------

torch.setdefaulttensortype("torch.DoubleTensor");

print("Loading data...");
-- Load softmax output for ALL questions
scores=torch.load("../../data/vtfq_premise_scores.t7")["softmax"]:type("torch.DoubleTensor");

-- Load questions json
local augmented_questions = json.load("../../data/vtfq_vqa_ques_augmented.json");
augmented_questions = augmented_questions["questions"];
-- local source_questions = utils.read_json("../../data/vtfq_vqa_anns_augmented.json")["questions"];

print("Data loading complete.");

local num_questions = 10793;

-- Create a mapping from question id to index
print("Mapping questions to index");
local qid2ix = {}
for i=1,#augmented_questions do
    qid2ix[augmented_questions[i]["question_id"]]=i
end
print("Mapping complete");

-- Setup variables
trainlabels = torch.DoubleTensor(num_questions,1);
trainqid = torch.IntTensor(num_questions, 1);
-- Hyperparam: crossvalidate this

normalizedaccuracy={0,0,0,0,0,0,0,0,0,0,0}
normalizedaccuracytrain={0,0,0,0,0,0,0,0,0,0,0}

for maxlength=2,4 do

	-- Create input channels for source and premise softmax outputs
	scores_source = torch.DoubleTensor(num_questions, 1000):zero();
	scores_premise = torch.DoubleTensor(num_questions, maxlength*1000):zero();

	print("Creating input...");
	-- Init input channels and ground truth
	local cix = 0;
	print(#scores)
	
	for i=1,#augmented_questions do

		-- Compute premise softmax average
		local qid = augmented_questions[i]["question_id"]
		if qid < offset then
			-- This is a source question
			cix = cix+1;

			-- 1 implies relevant, 2 irrelevant
			if augmented_questions[i]["label"] == 1 then
				trainlabels[cix] = 1
			else
				trainlabels[cix] = 0
			end
			
			local count = 0

			-- Get premise id from formula
			local pid = offset + (qid * 20) + (count + 1);
			-- print(pid);
			local ix = qid2ix[pid];
			-- if ix~=nil then
			-- 	print(ix);
			-- end

			local premise_scores = torch.DoubleTensor(maxlength*1000):zero();
			-- Loop over all premises
			while ix ~= nil do
				if count >= maxlength then
					break;
				end
	        	count = count+1;
	      
				premise_scores[{ {(1000*(count-1))+1, 1000*count} }] = scores[ix];
				pid = pid + 1
				ix = qid2ix[pid];		
			end
			trainqid[cix] = qid;
			scores_premise[cix] = premise_scores;
			scores_source[cix] = scores[i]
		end
	end


	-- Initialize training data
	traindata = torch.DoubleTensor(num_questions, 1000+(maxlength*1000)):zero();
	for i=1,traindata:size(1) do
		traindata[{{i}, {}}] = torch.cat(scores_source[i], scores_premise[i])
	end
	-- traindata[{{}, {1}, {}}] = scores_source
	-- traindata[{{}, {2}, {}}] = scores_premise

	print("Input created.")
	--repl()

	-----------------------------------------------------------------------------
	--- Architecture ---
	-----------------------------------------------------------------------------

	net=nn.Sequential()
	net:add(nn.Linear(1000+(maxlength*1000),100))
	net:add(nn.ReLU())
	net:add(nn.Linear(100,1))
	net:add(nn.Sigmoid())

	criterion=nn.BCECriterion()
	

	-- for tcount=1,5 do
	--	train_count=1

	print(string.format("Beginning training for length %d...", maxlength));

		ta=7195
	-----------------------------------------------------------------------------
	--- Training ---
	-----------------------------------------------------------------------------
			for i=1,2000 do
				--repl()
				pred=net:forward(traindata[{{1,ta},{}}])
				--pred=pred:gt(0.5)
				--repl()
				err=criterion:forward(pred, trainlabels[{{1,ta},{}}])
	--			print(err)
				net:zeroGradParameters()
		
				criterion_gradient=criterion:backward(pred, trainlabels[{{1,ta},{}}])
				net:backward(traindata[{{1,ta},{}}], criterion_gradient)

				net:updateParameters(0.01)
		
			end
	--	torch.save("savedmodels/rawscoremodel" .. tcount .. ".net", net)

	-----------------------------------------------------------------------------
	--- Evaluation ---
	-----------------------------------------------------------------------------

	--	net=torch.load("savedmodels/rawscoremodel1.net")
		train_pred=net:forward(traindata[{{1,ta},{}}])
		testlabels=trainlabels[{{1,ta},{}}]

		count=0
		zcount=0
		onecount=0
		foundzcount=0
		foundonecount=0
		foundonewrong=0
		foundzwrong=0
		--repl()

		-- TRAINING ACCURACY --
		print("Evaluating training accuracy..."); 
		thresh=0.2
		for i=1,ta do
			if train_pred[i][1]>=thresh then
				if testlabels[i][1]==1 then
					count=count+1
				end
			else
				if testlabels[i][1]==0 then
					count=count+1
				end
			end

			if testlabels[i][1]==0 then
				zcount=zcount+1		
				if train_pred[i][1]<thresh then
					foundzcount=foundzcount+1
				else
					foundonewrong=foundonewrong+1
				end
			else
				onecount=onecount+1
				if train_pred[i][1]>=thresh then
					foundonecount=foundonecount+1
				else
					foundzwrong=foundzwrong+1
				end
			end 
		end

		recall_irr=foundzcount/zcount
		recall_r=foundonecount/onecount

		-- normalizedaccuracytrain[tcount]=normalizedaccuracytrain[tcount]+(recall_irr+recall_r)/2


		-- TESTING ACCURACY --
		print("Evaluating test accuracy..."); 
		train_pred=net:forward(traindata[{{ta+1,(#traindata)[1]},{}}])
		testlabels=trainlabels[{{ta+1,(#traindata)[1]},{}}]
		
		predictions = torch.IntTensor(train_pred:size(1))
		for i=1,predictions:size(1) do
			if train_pred[i][1] > 0.2 then
				predictions[i]=1
			else
				predictions[i]=0
			end
		end

		print("Writing test predictions to file...")
		file = torch.DiskFile("predictions.txt", "w");
		file:writeString(tostring(predictions));
		file:close();

		-- file = torch.DiskFile("qid.txt", "w");
		-- file:writeObject(trainqid);
		-- file:close();
		
		count=0
		zcount=0
		onecount=0
		foundzcount=0
		foundonecount=0
		foundonewrong=0
		foundzwrong=0

		thresh=torch.mean(train_pred)
		for i=1,((#traindata)[1]-ta-1) do
			if train_pred[i][1]>=thresh then
				if testlabels[i][1]==1 then
					count=count+1
				end
			else
				if testlabels[i][1]==0 then
					count=count+1
				end
			end

			if testlabels[i][1]==0 then
				zcount=zcount+1		
				if train_pred[i][1]<thresh then
					foundzcount=foundzcount+1
				else
					foundonewrong=foundonewrong+1
				end
			else
				onecount=onecount+1
				if train_pred[i][1]>=thresh then
					foundonecount=foundonecount+1
				else
					foundzwrong=foundzwrong+1
				end
			end 
		end

		accuracy=count/((#traindata)[1]-7195)
		-- FoR CLASS IRRELEVANT (CLASS 0) --
		precision_irr=foundzcount/(foundzcount+foundzwrong)
		recall_irr=foundzcount/zcount

		-- FOR CLASS RELEVANT (CLASS 1) --
		precision_r=foundonecount/(foundonecount+foundonewrong)
		recall_r=foundonecount/onecount

		print("Prec, Recall for Irrelevant:")
		print(precision_irr)
		print(recall_irr)

		print("Prec, Recall for Relevant:")
		print(precision_r)
		print(recall_r)

		print("")

	normalizedaccuracy[maxlength]=(recall_irr+recall_r)/2

--	train_count=train_count+1
	
-- end
end
print(normalizedaccuracy)

print("Logging CROSSVAL results to file...")
json.save("results.json", normalizedaccuracy);
repl()

-- require "../utils/metric"
-- print(metric.precision(-(entropy-expected_entropy),applicable:gt(0.5):double()))
-- print(metric.precision(-(entropy_model_averaging-entropy),applicable:gt(0.5):double()))
-- print(metric.precision(-(entropy),applicable:gt(0.5):double()))


