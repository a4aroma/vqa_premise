---------------------------------------------------------------------------------------------------
-- Utils
---------------------------------------------------------------------------------------------------
local cjson=require "cjson"
-- local w2vutils=require "w2vutils"
repl=require "trepl"
local utils = {}
local offset = 10000000
function utils.read_json(path)
  local file = io.open(path, 'r')
  local text = file:read()
  file:close()
  local info = cjson.decode(text)
  return info
end

function utils.write_json(path, j)
  local text = cjson.encode(j)
  local file = io.open(path, 'w')
  file:write(text)
  file:close()
end

function utils.getopt(opt, key, default_value)
  if default_value == nil and (opt == nil or opt[key] == nil) then
    error('error: required key ' .. key .. ' was not provided in an opt.')
  end
  if opt == nil then return default_value end

  local v = opt[key]
  if v == nil then v = default_value end
  return v
end

---------------------------------------------------------------------------------------------------
-- Data Loader class for failure prediction model
---------------------------------------------------------------------------------------------------

local DataLoader = torch.class("DataLoader")

function DataLoader:__init(opt)
	
	print("Dataloader loading input softmax t7: ", opt.input_softmax_t7)
	self.scores = torch.load(opt.input_softmax_t7)["softmax"]:type("torch.DoubleTensor")

	print("Dataloader loading questions file ", opt.input_questions)
	self.augmented_questions = utils.read_json(opt.input_questions)["results"]
	self.num_questions = 0
  -- print(self.num_questions)
  for i=1,#self.augmented_questions do
    if self.augmented_questions[i]["question_id"] < offset then
      self.num_questions = self.num_questions + 1
    end
  end
  -- print(self.num_questions)
	self.trainlabels = torch.DoubleTensor(self.num_questions,1)
	self.bin_length = opt.bin_length 
	self.nonbin_length = opt.nonbin_length
	print("Dataloader loading complete")

	-- Create input channels for source and premise softmax outputs
	-- scores_source = torch.DoubleTensor(self.num_questions, 1300):zero()
	scores_source = torch.DoubleTensor(self.num_questions, 1000):zero()
	-- scores_premise = torch.DoubleTensor(self.num_questions, (self.bin_length + self.nonbin_length) * 1300):zero()
	scores_premise = torch.DoubleTensor(self.num_questions, (self.bin_length + self.nonbin_length) * 1000):zero()

	local cix = 0

	-- Create a mapping from question id to index
	local qid2ix = {}
  local augmented_questions = self.augmented_questions
	for i=1,#augmented_questions do
	    qid2ix[augmented_questions[i]["question_id"]]=i
	end

  -- local words_per_question = 7 -- hardcoded for now
	print("Dataloader setting up input")
	for i=1,#augmented_questions do

		local qid = self.augmented_questions[i]["question_id"]
    -- get question
    local question = self.augmented_questions[i]["question"]
    -- lstm_clones = {}
    -- lstm_clones = utils.clone_many_times(checkpoint.protos.lstm, words_per_question)


		if qid < offset then -- This is a source question
			
			cix = cix+1

			if augmented_questions[i]["label"] == 1 then -- 1 implies relevant, 2 irrelevant
				self.trainlabels[cix] = 1
			else
				self.trainlabels[cix] = 0
			end
			
			local bincount = 0
			local nonbincount = 0

			-- Get premise id from formula
			local pid = offset + (qid * 20) + 1
			local ix = qid2ix[pid]

			-- local premise_scores_bin = torch.DoubleTensor(self.bin_length*1300):zero()
			-- local premise_scores_nonbin = torch.DoubleTensor(self.nonbin_length*1300):zero()
			local premise_scores_bin = torch.DoubleTensor(self.bin_length*1000):zero()
			local premise_scores_nonbin = torch.DoubleTensor(self.nonbin_length*1000):zero()
			-- Loop over all premises
			while ix ~= nil do
				if augmented_questions[i]["type"] == "bin" then
					if bincount >= self.bin_length then
						break
					else
			      		--- Premise Channel 1: Binary
						bincount = bincount+1
						-- local premisebinq_w2v = w2vutils:word2vec(augmented_questions[ix]["question"]):type("torch.DoubleTensor")
						-- premise_scores_bin[{ {(1300*(bincount-1))+1, 1300*bincount} }] = torch.cat(premisebinq_w2v, self.scores[ix])
						premise_scores_bin[{ {(1000*(bincount-1))+1, 1000*bincount} }] = self.scores[ix]
					end	
				elseif augmented_questions[i]["type"] == "nonbin" then
					if nonbincount >= self.nonbin_length then
						break
					else
			      		--- Premise Channel 2: Nonbinary
						nonbincount = nonbincount+1
						-- local premisenonbinq_w2v = w2vutils:word2vec(augmented_questions[ix]["question"]):type("torch.DoubleTensor")
						-- premise_scores_nonbin[{ {(1300*(nonbincount-1))+1, 1300*nonbincount} }] = torch.cat(premisenonbinq_w2v, self.scores[ix])
						premise_scores_nonbin[{ {(1000*(nonbincount-1))+1, 1000*nonbincount} }] = self.scores[ix]
					end
				end
	      		
				pid = pid + 1
				ix = qid2ix[pid]		
			end
			
			-- sourceq_w2v = w2vutils:word2vec(augmented_questions[i]["question"]):type("torch.DoubleTensor")

			scores_premise[cix] = torch.cat(premise_scores_bin, premise_scores_nonbin)
			-- scores_source[cix] = torch.cat(sourceq_w2v, self.scores[i])
			scores_source[cix] = self.scores[i]
		end
	end

	-- Initialize training data
	self.trainsize = 1*self.num_questions/2
  self.valsize = 1*self.num_questions/6
	self.testsize = self.num_questions-self.trainsize-self.valsize
  
	-- self.alldata = torch.DoubleTensor(self.num_questions, 1300+((self.bin_length+self.nonbin_length)*1300)):zero()
	self.alldata = torch.DoubleTensor(self.num_questions, 1000+((self.bin_length+self.nonbin_length)*1000)):zero()
  -- print(self.alldata:size())
  -- print(scores_source:size())
  -- print(scores_premise:size())
  for i=1,self.num_questions do
    self.alldata[i]=torch.cat(scores_source[i], scores_premise[i])
  end

  
  -- Let's get the splits 
  self.iterators = {}
  self.iterators[1] = 1					-- 1: train
  self.iterators[2] = self.trainsize -- 2: val
  self.iterators[3] = self.trainsize + self.valsize	-- 3: test

  self.startix = {}
  for j,x in ipairs(self.iterators) do self.startix[j] = x end
  -- print(self.startix)

  self.maxix = {}
  self.maxix[1] = self.trainsize
  self.maxix[2] = self.trainsize +  self.valsize
  self.maxix[3] = self.num_questions

	print("Input collected.")
	collectgarbage()
end
function DataLoader:resetIterator(split)
  -- print(self.startix[split], split)
  self.iterators[split] = self.startix[split]
end
  
function DataLoader:getNumQuestions(split)
  return self.maxix[split] - self.startix[split] + 1
end

function DataLoader:getData()
  return self.alldata
end

function DataLoader:getLabels()
  return self.trainlabels
end

function DataLoader:getBatch(opt)
    local batch_size = utils.getopt(opt, 'batch_size', 256)
    local split = utils.getopt(opt, 'split', 1)

    local max_index = self.maxix[split]
    local infos = {}

    local data = {}
    data.features = torch.DoubleTensor(batch_size, self.alldata:size(2))
    data.trainlabels = torch.DoubleTensor(batch_size, self.trainlabels:size(2))
    -- print(self.iterators[split])
    for i=1,batch_size do
        local ri = self.iterators[split] -- get next index from iterator
        -- print(ri)
        -- print(self.alldata[ri])
        data.features[i] = self.alldata[ri]
        data.trainlabels[i] = self.trainlabels[ri]
        -- repl()
        local ri_next = ri + 1 -- increment iterator
        if ri_next > max_index then 
        	ri_next = self.startix[split]
        end
        self.iterators[split] = ri_next
    end

    return data
end
