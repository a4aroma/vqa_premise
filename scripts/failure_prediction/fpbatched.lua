-----------------------------------------------------------------------------
-- Todo: Use padding + clipping for premise softmax
-- Use separate channels for binary / non binary questions
-----------------------------------------------------------------------------

require "nn"
require "DataLoader"
local cjson = require "cjson"
repl=require "trepl"
local offset = 10000000

local utils = {}

function utils.write_json(path, j)
  local text = cjson.encode(j)
  local file = io.open(path, 'w')
  file:write(text)
  file:close()
end
local accuracy_history = {}
-----------------------------------------------------------------------------
--- Input arguments and options ---
-----------------------------------------------------------------------------

cmd = torch.CmdLine()
cmd:text()
cmd:text("Train a failure prediction model for VQA")
cmd:text()
cmd:text("Options")

-- Data input
cmd:option('-input_softmax_t7','../../data/softmaxout.t7','path to the t7 containing VQA output log softmax')
cmd:option('-input_questions','../../data/results_new.json','path to the h5file containing the image feature')
cmd:option('-batch_size',256,'set batch size')

-- Optimization
cmd:option('-optim','rmsprop','what update to use? rmsprop|sgd|sgdmom|adagrad|adam')
cmd:option('-learning_rate',4e-4,'learning rate')
cmd:option('-learning_rate_decay_start', 0, 'at what iteration to start decaying learning rate? (-1 = dont)')
cmd:option('-learning_rate_decay_every', 300, 'every how many epoch thereafter to drop LR by 0.1?')
cmd:option('-optim_alpha',0.99,'alpha for adagrad/rmsprop/momentum/adam')
cmd:option('-optim_beta',0.995,'beta used for adam')
cmd:option('-optim_epsilon',1e-8,'epsilon that goes into denominator in rmsprop')
cmd:option('-max_iters', 20000, 'max number of iterations to run for (-1 = run forever)')
cmd:option('-iterPerEpoch', 1200)

-- Evaluation / Checkpointing
cmd:option('-save_checkpoint_every', 1000, 'how often to save a model checkpoint?')
cmd:option('-losses_log_every', 1000, 'How often do we save losses?')
cmd:option('-checkpoint_path', 'save/train_mlp', 'folder to save checkpoints into (empty = this folder)')

-- misc
cmd:option('-backend', 'cudnn', 'nn|cudnn')
cmd:option('-gpuid', -1, 'which gpu to use. -1 = use CPU')
cmd:option('-seed', 123, 'random number generator seed to use')

cmd:text()

-------------------------------------------------------------------------------
-- Basic Torch initializations
-------------------------------------------------------------------------------
local opt = cmd:parse(arg)
torch.manualSeed(opt.seed)
print(opt)
torch.setdefaulttensortype('torch.DoubleTensor') -- for CPU

if opt.gpuid >= 0 then
  require 'cutorch'
  require 'cunn'
  if opt.backend == 'cudnn' then 
  require 'cudnn' 
  end
end

opt = cmd:parse(arg)

local opt = cmd:parse(arg)

-------------------------------------------------------------------------------
-- Create the Data Loader instance
-------------------------------------------------------------------------------
local bin_length = 2
local nonbin_length = 2
local loader = DataLoader{input_softmax_t7 = opt.input_softmax_t7, input_questions = opt.input_questions, bin_length = bin_length, nonbin_length = nonbin_length}

-----------------------------------------------------------------------------
--- Architecture ---
-----------------------------------------------------------------------------

net=nn.Sequential()

-- net:add(nn.Linear(1300+(bin_length+nonbin_length)*1300,100))
net:add(nn.Linear(1000+(bin_length+nonbin_length)*1000,100))
net:add(nn.ReLU())
net:add(nn.Linear(100,1))
net:add(nn.Sigmoid())

criterion=nn.BCECriterion()

-----------------------------------------------------------------------------
--- Loss Function ---
-----------------------------------------------------------------------------

local function lossFun()
	-- get batch of data
    local data = loader:getBatch{batch_size=opt.batch_size, split=1}
	if opt.gpuid >=0 then
		-- figure this out
		data.features = data.features:cuda()
		data.trainlabels = data.trainlabels:cuda()
	end
	
	-- forward pass
	pred=net:forward(data.features)
	local loss = criterion:forward(pred, data.trainlabels)
	net:zeroGradParameters()

	-- backward pass
	criterion_gradient=criterion:backward(pred, data.trainlabels)
	net:backward(data.features, criterion_gradient)

	return loss
end

local function eval_split(split)
  -- Evaluate on selected split
  loader:resetIterator(split)
  local total_correct = 0
  local total_num = loader:getNumQuestions(split)
  -- print(total_num)
  predictions = torch.IntTensor(total_num)
  for i=1, total_num, opt.batch_size do
    local data = loader:getBatch{batch_size = opt.batch_size, split = split}
    if opt.gpuid >= 0 then
      data.features = data.features:cuda()
      data.trainlabels = data.trainlabels:cuda()
    end
    pred = net:forward(data.features)
    thresh = torch.mean(pred) 
    local correct = 0
    pred_thresh = torch.IntTensor(pred:size(1))
    for j=1,pred:size()[1] do
      if pred[j][1] > thresh then
        pred_thresh[j] = 1
      else
        pred_thresh[j] = 0
      end
      if pred_thresh[j] == data.trainlabels[j][1] then
        correct = correct + 1
      end

    end
    total_correct = total_correct + correct
    end_index = i+opt.batch_size-1
    len = opt.batch_size
    if i+opt.batch_size-1 > total_num then
      end_index = total_num
      len = total_num - i + 1
    end
    -- print(end_index)
    predictions[{ {i, end_index} }] = pred_thresh[{ {1, len} }]
  end
  print("total correct: ", total_correct, " total_num:", total_num)
  return predictions, total_correct / total_num 
end
print("Beginning training...")

-----------------------------------------------------------------------------
--- Training ---
-----------------------------------------------------------------------------
local learning_rate = opt.learning_rate
local iter = 0
local accuracy_history = {}
local ave_loss = 0
local decay_factor= math.exp(math.log(0.1)/opt.learning_rate_decay_every/opt.iterPerEpoch)
local validation_history = {}
local loss_history = {}
while true do
  
 	local losses = lossFun()
  ave_loss = ave_loss + losses
  
  if iter % opt.losses_log_every == 0 then
    ave_loss = ave_loss / opt.losses_log_every
    loss_history[iter] = ave_loss
    print(string.format("Iteration %d: %f, %f", iter, ave_loss, learning_rate))
    ave_loss = 0
  -- end
  -- if iter % opt.save_checkpoint_every == 0 or iter == opt.max_iters then
        
		-- print(string.format("Iteration %d", iter))
    dummy, train_accu = eval_split(1)
    dummy, val_accu = eval_split(2)
    print('Validation accuracy ', val_accu, ' Train accuracy ', train_accu)
    accuracy_history[iter] = train_accu
    validation_history[iter] = val_accu
    local checkpoint_path = opt.checkpoint_path .. '_iter' .. iter .. '_checkpoint.json'
    print(checkpoint_path)
    -- save checkpoint here ONLY if val loss decreases

    local checkpoint = {}
    checkpoint.opt = opt
    checkpoint.iter = iter
    checkpoint.loss_history = loss_history
    checkpoint.accuracy_history = accuracy_history
    checkpoint.validation_history = validation_history
    -- write checkpoint here
    utils.write_json(checkpoint_path, checkpoint)
    print('Wrote json checkpoint to ' .. checkpoint_path .. '.json')
  end
	-- decay learning rate
	if iter > opt.learning_rate_decay_start and opt.learning_rate_decay_start >= 0 then
    	learning_rate = learning_rate * decay_factor -- set the decayed rate
  end

  	-- perform a parameter 	
	net:updateParameters(learning_rate)
	
	iter = iter+1
	if opt.max_iters >0 and iter >= opt.max_iters then break end -- stopping criterion
end

-----------------------------------------------------------------------------
--- Evaluation ---
-----------------------------------------------------------------------------

-- TRAINING ACCURACY -- 
print("Evaluating training accuracy...") 
dummy, acc = eval_split(1)
print(string.format("Training accuracy: %f", acc))

-- TESTING ACCURACY --
print("Evaluating test accuracy...") 
predictions, test_accuracy = eval_split(3)
print("Test accuracy: ", test_accuracy)
file=torch.DiskFile("predictions.txt", "w")
file:writeString(tostring(predictions))
file:close()

repl()
