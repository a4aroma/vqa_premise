# coding: utf-8
import json
import datetime
import os
from collections import Counter
import random
import sys
import argparse
import nltk
import nltk.data

tagger = nltk.data.load("taggers/maxent_treebank_pos_tagger/english.pickle")

parser = argparse.ArgumentParser(description='Get Answer Type for VQA')
parser.add_argument('--dataset', '-d', default='train2014', help='dataset - train2014/val2014')


args = parser.parse_args()
dataset = args.dataset

print 'Dataset: ' + dataset

filename = "mscoco_" + str(dataset) + "_annotations_aug.json"

with open(filename) as f:
	d = json.load(f)

annot = d['annotations']

for i in range(len(d['annotations'])):
    wordTags = [x[1] for x in tagger.tag(nltk.word_tokenize(d['annotations'][i]['multiple_choice_answer']))]
    if d['annotations'][i]['multiple_choice_answer']=='yes' or d['annotations'][i]['multiple_choice_answer']=='no': 
        tag = 'yes/no'
    elif 'LS' in wordTags or 'CD' in wordTags: 
        tag = 'number'
    else: 
        tag = 'other'
    d['annotations'][i]['answer_type'] = tag

with open(filename, 'w') as f:
	json.dump(d, f)
