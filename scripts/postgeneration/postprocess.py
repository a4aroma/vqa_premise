
import sys
import json
import postprocess

"""
Generate SPICE Input from generated premise questions
"""

if __name__ == "__main__":
	path_to_questions = "questions/vqa_val_questions.json"
	path_to_output = "postprocessed/questions.json"

	with open(path_to_questions) as ip, open(path_to_output, 'w') as op:
		data = json.loads(ip.read())
		results = []
		for item in data:
			question = item["question"]
			tuples = item["tuples"]
			for tuple in tuples:
				premise_questions = tuple["premise_questions"]

				for premise_question in premise_questions:
					# print premise_question
					result = {}
					result["refs"] = []
					result["refs"].append(question)
					if premise_question == [] or not premise_question:
						continue
					else:
						result["test"] = premise_question["q"]
						result["image_id"] = premise_question["q"] + "_" + premise_question["a"] + "_" + str(premise_question["question_id"])
					results.append(result)
			
		json.dump(results, op, indent=4, separators=(',', ': '))	
	# Run spice on input