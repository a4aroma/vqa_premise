import json

if __name__ == "__main__":
	path_to_questions = "questions/vqa_val_questions.json"
	path_to_input = "postprocessed/questions_spice.json"
	path_to_output = "final/questions.json"
	filtered_questions = []

	with open(path_to_input, "r") as f1, open(path_to_questions, "r") as f2:
		spice_data = json.loads(f1.read())
		questions = json.loads(f2.read())
		prev = 0
		i = 0
		for question_data in questions:
			
			tuples = question_data["tuples"]
			j = 0
			for tuple in tuples:
				
				premise_questions = tuple["premise_questions"]
				spice_subset = spice_data[prev:prev+len(premise_questions)]
				prev = prev + len(premise_questions)

				rejected = []

				for item in spice_subset:
					fscore = item["scores"]["All"]["f"]
					print item["image_id"]

					if fscore == 1: # todo: Finetune this value
						# This is the source question, add it to rejected pile
						qa = item["image_id"].split("_")
						reject = {
							"q": qa[0],
							"a": qa[1],
							"question_id": int(qa[2])
						}
						rejected.append(reject)

						if reject in questions[i]["tuples"][j]["premise_questions"]:
							questions[i]["tuples"][j]["premise_questions"].remove(reject)
				if len(rejected) > 0:
					questions[i]["tuples"][j]["rejected_questions"] = rejected
				j += 1
			i += 1

	with open(path_to_output, "w") as f1:
		json.dump(questions, f1, indent=4, separators=(',', ': '))
