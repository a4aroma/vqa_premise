Post premise question generation pipeline:

1) premise_to_spice.py: Converts json with generated premise questions to SPICE input
2) Run SPICE on the output of 1, and store results under '/postprocessed'
3) filter_false_premise.py: Uses the output of 2 to filter out premise questions with f-score 1 overlap with source question,
 and writes output to '/final'
3) create_test_input.py: Generates VQA question and annotations files from the output of 3, and stores results under '/augmented'