import os, sys
import json
import copy

generated_questions_file = "final/questions.json"
source_questions_file = "data/OpenEnded_mscoco_train2014_questions.json"
source_annotations_file = "data/mscoco_train2014_annotations.json"
augmented_data_restricted_path = "augmented/OpenEnded_mscoco_train2014_questions_augmented.json"
augmented_annotations_restricted_path = "augmented/OpenEnded_mscoco_train2014_annotations_augmented.json"

# Input the VQA question types file
with open("mscoco_question_types.txt") as f:
    types = f.read().splitlines()
    types.sort(lambda x, y: cmp(len(x), len(y)), reverse=True)

# 'none of the above' added
types.append('none of the above')

# Note - Could add synonyms to answer set instead of duplicating 
# Some models do predict synonyms 
def replicate_answers(ans):
    answers = []
    for i in range(1, 11):
        answers.append({
            "answer": ans,
            "answer_confidence": "yes",
            "answer_id": i
        })
    return answers

# Function to determine appropriate VQA answer type 
# Only 'yes/no' or 'other' generaated
def determine_ans_type(answer):
    if answer.lower() == 'yes' or answer.lower() =='no':
        return 'yes/no'
    else:
        return 'other'

# Function modified to return appropriate VQA question type
def determine_ques_type(question):
    # Return the answer and question types for the given question
    global types
    for ts in types:
        question = ' '.join(question.split())
        if question.lower().startswith('what is the woman?'):
            return 'what is the woman'
        if question.lower().startswith('what color is the?'):
            return 'what color is the'
        if question.lower().startswith('what color is?'):
            return 'what color is'
        if question.lower().startswith('what room is?'):
            return 'what room is'
        if question.lower().startswith('what is the name?'):
            return 'what is the name'
        if question.lower().startswith('what is there?'):
            return 'what is'
        if question.lower().startswith('what color is there'):
            return 'what color is'
        if question.lower().startswith('what is this?'):
            return 'what is this'
        if question.lower().startswith(ts + ' '):
            # print qa[0], " in ", question
            return ts

    return 'none of the above'

with open(generated_questions_file, "r") as f1, open(source_questions_file, "r") as f2, \
     open(source_annotations_file, "r") as f3:
    
    source_data = json.loads(f2.read())

    source_questions = source_data["questions"]
    source_annotations = (json.loads(f3.read()))

    # Map qid to image id
    q2im = {}
    for i in range(0,len(source_questions)):
        q2im[source_questions[i]["question_id"]] = source_questions[i]["image_id"]

    q2ix = {}
    for i in range(0,len(source_questions)):
        q2ix[source_questions[i]["question_id"]] = i

    augmented_data_restricted = copy.deepcopy(source_data)

    augmented_annotations_restricted = copy.deepcopy(source_annotations)

    generated_questions = json.loads(f1.read())

    augmented_data_restricted["questions"] = []
    augmented_annotations_restricted["annotations"] = []
    for question in generated_questions:
        
        image_id = q2im[question["question_id"]]
        ix = q2ix[question["question_id"]]

        augmented_data_restricted["questions"].append(copy.deepcopy(source_questions[ix]))
        augmented_annotations_restricted["annotations"].append(source_annotations["annotations"][ix])
        for tuple in question["tuples"]:
            for premise_question in tuple["premise_questions"]:

                # Append to questions

                # print i
                premise_question_entry = {}
                premise_question_entry["image_id"] = image_id

                # Note: Remove null from questions file
                if not premise_question or not premise_question["q"]:
                    print question["question_id"]
                premise_question_entry["question"] = premise_question["q"]
                
                # SETTING 1

                premise_question_entry["question_id"] = premise_question["question_id"]
                augmented_data_restricted["questions"].append(copy.deepcopy(premise_question_entry))

                premise_annotation_entry = {}
                ques = determine_ques_type(premise_question["q"].lower())
                premise_annotation_question_type = ques
                atype = determine_ans_type(premise_question["a"])
                premise_annotation_answer_type = atype
                # Answer type is either 'yes/no' or 'other'. No 'number' type premise answers are currently generated
                premise_annotation_entry["answer_type"] = premise_annotation_answer_type
                premise_annotation_entry["question_type"] = premise_annotation_question_type

                premise_annotation_entry["answers"] = replicate_answers(premise_question["a"])
                premise_annotation_entry["question_id"] = premise_question["question_id"]
                premise_annotation_entry["image_id"] = image_id
                premise_annotation_entry["multiple_choice_answer"] = premise_question["a"]
                
                augmented_annotations_restricted["annotations"].append(copy.deepcopy(premise_annotation_entry))


with open(augmented_data_restricted_path, "w") as f1, \
     open(augmented_annotations_restricted_path, "w") as f2:
    json.dump(augmented_data_restricted, f1, indent=4, separators=(',', ': '))
    json.dump(augmented_annotations_restricted, f2, indent=4, separators=(',', ': '))
