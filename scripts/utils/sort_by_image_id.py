import os, sys
import json
import copy

questions_file = "OpenEnded_mscoco_val2014_questions_augmented.json"
annotations_file = "OpenEnded_mscoco_val2014_annotations_augmented.json"

questions_op_file = "OpenEnded_mscoco_val2014_questions_augmented_new_sorted.json"
annotations_op_file = "OpenEnded_mscoco_val2014_annotations_augmented_new_sorted.json"
with open(questions_file, "r+") as f1, open(annotations_file, "r+") as f2, \
	 open(questions_op_file, "w") as f3, open(annotations_op_file, "w") as f4:
	questions_obj = json.loads(f1.read())
	questions = questions_obj["questions"]
	
	annotations_obj = json.loads(f2.read())
	annotations = annotations_obj["annotations"]

	questions.sort(key=lambda q: q["image_id"], reverse=False)
	annotations.sort(key=lambda a: a["image_id"], reverse=False)

	questions_obj["questions"] = questions
	annotations_obj["annotations"] = annotations

	# f1.seek(0)
	# f2.seek(0)
	json.dump(questions_obj, f3)
	json.dump(annotations_obj, f4)
	# f1.truncate()
	# f2.truncate()