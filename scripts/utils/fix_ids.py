import json
with open("OpenEnded_mscoco_co-atten_results.json", "r") as f1, open("../Questions/Questions_Val_mscoco/OpenEnded_mscoco_val2014_questions_augmented.json", "r") as f2:
	results = json.loads(f1.read())
	correct = json.loads(f2.read())["questions"]
	for i in range(0, len(results)):
		results[i]["question_id"] = int(correct[i]["question_id"])
	with open("final.json", "w") as f:
		json.dump(results, f, indent=4, separators=(',', ': '))