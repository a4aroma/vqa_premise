import json
from collections import defaultdict
from collections import Counter

# Load all files required
with open("annotations/mscoco_train2014_annotations_aug.json") as f1, \
        open("annotations/mscoco_val2014_annotations_aug.json") as f2, \
        open("vqa_raw_train.json") as f3, \
        open("/home/akrit/VQA_LSTM/Comp/Baseline/data/annotations/mscoco_train2014_compositional_v2_annotations.json") as f4, \
        open("annotations/OpenEnded_mscoco_train2014_questions_aug.json") as f5, \
        open("annotations/OpenEnded_mscoco_val2014_questions_aug.json") as f6:
            aug_tr = json.load(f1)
            aug_val = json.load(f2)
            raw = json.load(f3)
            comp = json.load(f4)
            ques_tr = json.load(f5)
            ques_val = json.load(f6)
            print 'Loaded all necessary files'

aug_annot = aug_tr['annotations'] + aug_val['annotations']
aug_ques = ques_tr['questions'] + ques_val['questions']
comp = comp['annotations']
print 'Total No. of Augmented questions loaded - ' + str(len(aug_ques))
print 'Total No. of Augmented annotations loaded - ' + str(len(aug_annot))
print 'Note - The above two numbers must match'
print 'Total number of questions in current split - ' + str(len(raw))
print 'Total number of annotations loaded from current split - ' + str(len(comp))
print 'Note - The above two numbers must match'

print 'Premise Questions offset is 5819292'

# Get the image ids that are in the split
imgs = list(set([i['image_id'] for i in comp]))
img_split = {}
for i in comp:
        img_split[i['image_id']] = i['coco_split']
print 'No. of unique images found - ' + str(len(imgs))

# Keep premise questions only and filter out binary questions
aug_annot = [i for i in aug_annot if i['question_id'] > 5819292]
aug_annot = [i for i in aug_annot if i['answer_type'] != 'yes/no']
aug_ques = [i for i in aug_ques if i['question_id'] > 5819292]

d = defaultdict(list)

for i in aug_annot:
    d[i['image_id']].append(i)

aug_annot = []
for i in imgs:
    aug_annot.extend(d[i])

# Remove duplicate question ids
ques_ids = [i['question_id'] for i in aug_annot]
ques_ids = Counter(ques_ids)
ques_ids = [i for i in ques_ids if ques_ids[i] == 1]
print 'Total number of premise question_ids (without duplicates) - ' + str(len(ques_ids))

# Create temporary dictionary for questions
temp_q = {}
for i in aug_ques:
        temp_q[i['question_id']] = i

temp_a = {}
for i in aug_annot:
        temp_a[i['question_id']] = i

imdir='%s/COCO_%s_%012d.jpg'
aug = {}
for i in ques_ids:
        aug[i] = {}
        aug[i]['image_id'] = temp_q[i]['image_id']
        aug[i]['ques_id'] = i
        aug[i]['question'] = temp_q[i]['question']
        aug[i]['ans'] = temp_a[i]['multiple_choice_answer']
        aug[i]['img_path'] = imdir%(img_split[temp_q[i]['image_id']], img_split[temp_q[i]['image_id']], temp_q[i]['image_id'])

print 'Number of filtered premise questions - ' + str(len(aug))

for i in aug.items():
        raw.append(i[1])

for i in raw:
        i.pop('image_id', None)

check = []
for i in raw:
        check.append(i['img_path'])
check = list(set(check))

print 'Check for match - ' + str(len(imgs)) + ' == ' + str(len(check))

with open("vqa_raw_train_prem.json", 'w') as f:
        json.dump(raw,f)

print 'Generating new pre-processesed json file... (vqa_raw_train_prem.json)'
